const mix = require('laravel-mix');
const tailwindcss = require("tailwindcss");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js/app.js")
    .sass("resources/sass/app.sass", "public/css")
    // common javascipt files
    .babel(
        [
            "public/js/common/jquery.min.js",
            "public/js/common/popper.min.js",
            "public/js/common/bootstrap.min.js",
            "public/js/common/bootstrap.min.js",
        ],
        "public/js/common.js"
    )
    // javscript files only included on admin pages
    .babel(
        [
            "public/js/admin/grasp_mobile_progress_circle-1.0.0.min.js",
            "public/js/admin/jquery.cookie.js",
            "public/js/admin/jquery.mCustomScrollbar.concat.min.js",
            "public/js/admin/front.js",
        ],
        "public/js/admin.js"
    )
    //common css files will be included on every page
    .styles(["public/css/common/bootstrap.min.css"], "public/css/common.css") //common css files will be included on every page
    //common css files will be included on every page
    // css files only incuded in admin pages
    .styles(
        [
            "public/css/common/font-awesome.min.css",
            "public/css/admin/fontastic.css",
            "public/css/admin/grasp_mobile_progress_circle-1.0.0.min.css",
            "public/css/admin/jquery.mCustomScrollbar.css",
            "public/css/admin/custom.css",
            "public/css/admin/style.default.css",
        ],
        "public/css/admin.css"
    )
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.config.js') ],
    });

if (mix.inProduction()) {
   mix.version();
}
