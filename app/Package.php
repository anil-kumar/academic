<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use Notifiable, Sortable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'no_of_session', 'package_price', 'session_price', 'date_from', 'date_to', 'session_duration'
    ];


    protected $casts = [
        'date_from' => 'datetime',
        'date_to' => 'datetime',
    ];

    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }

    public function getDateFromAttribute($value)
    {
        return date('Y-m-d',strtotime($value) );
    }

    public function getDateToAttribute($value)
    {
        return date('Y-m-d', strtotime($value));
    }

    public function availability()
    {
        return $this->hasMany('App\PackageAvailability');
    }
}
