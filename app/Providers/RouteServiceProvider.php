<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/dashboard';
    public const ADMIN_HOME = '/admin/users';
    public const USER_LOGIN = '/login';
    public const ADMIN_LOGIN = '/admin';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();

        Route::bind('user', function($value, $route)
        {
            $hashids = new \Hashids\Hashids(config('app.key'));
            return $hashids->decode($value)[0];
        });

        Route::bind('package', function($value, $route)
        {
            $hashids = new \Hashids\Hashids(config('app.key'));
            return $hashids->decode($value)[0];
        });

        Route::bind('task', function($value, $route)
        {
            $hashids = new \Hashids\Hashids(config('app.key'));
            return $hashids->decode($value)[0];
        });

        Route::bind('quote_video', function($value, $route)
        {
            $hashids = new \Hashids\Hashids(config('app.key'));
            return $hashids->decode($value)[0];
        });

    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
