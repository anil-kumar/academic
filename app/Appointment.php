<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Appointment extends Model
{
    
    use Notifiable, Sortable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'package_id', 'appointment_date', 'appointment_time'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'appointment_date' => 'datetime',
    ];

    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id', 'status'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function package()
    {
        return $this->belongsTo('App\Package', 'package_id')->withTrashed();
    }

    public function getStatusAttribute()
    {   
        $tz='UTC';
        $currentTime = Carbon::now($tz);
        $appointmentTime = Carbon::createFromTimeString($this->appointment_date.' '.$this->appointment_time);
        return ($currentTime->greaterThan($appointmentTime))?"complete":"upcoming";
    }

}
