<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class UserProfile extends Model
{   
	use Notifiable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','first_name', 'last_name', 'parent_name', 'parent_email', 'phone_no', 'address', 'city', 'state_zip', 'photo'
    ];

    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id', 'thumb_image'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }


    public function getThumbImageAttribute()
    {
    	if($this->photo) {
    		$thumbPath= 'avatar/'.$this->user_id.'/thumb_'.$this->photo;
            if (Storage::disk('public')->exists($thumbPath)) {
            	return Storage::url($thumbPath);
            }
    	}
    	return '/images/profile.png';
    }
}
