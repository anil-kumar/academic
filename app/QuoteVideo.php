<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuoteVideo extends Model
{
    use Notifiable, Sortable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'quote', 'title', 'video', 'display_at', 'disk'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        // 'display_at' => 'datetime',
    ];


    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }
}
