<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use Notifiable, Sortable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'user_id'
    ];

    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
