<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Kyslik\ColumnSortable\Sortable;
use Storage;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id', 'full_name', 'thumb_image', 'photo'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }

    public function getFullNameAttribute() {
        if($this->profile) {
            return ucwords($this->profile->first_name.' '.$this->profile->last_name);
        }
        return $this->name;
    }

    public function getThumbImageAttribute()
    {
        if(!empty($this->profile)) {
            return $this->profile->thumb_image;
        }
        return '/images/profile.png';
    }

    public function getPhotoAttribute()
    {
        if(!empty($this->profile)) {
           return $this->profile->photo;
        }
        return '/images/profile.png';
    }

    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    public function qualification()
    {
        return $this->hasOne('App\UserQualification');
    }

    public function userForm()
    {
        return $this->hasOne('App\UsersForm');
    }
}
