<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersForm extends Model
{
    protected $table = 'users_forms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'form_id'
    ];

    /**
     * The accessors attributes that should be available in json.
     *
     * @var array
     */
    protected $appends = ['hash_id'];

    public function getHashIdAttribute() {
        $hashids = new \Hashids\Hashids(config('app.key'));
        return $hashids->encode($this->id);
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function form()
    {
        return $this->belongsTo('App\Form');
    }
}
