<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\QuoteVideo;
use Illuminate\Support\Facades\Log;
use FFMpeg\Exception\RuntimeException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class ProcessQuoteVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $quote;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(QuoteVideo $quote)
    {
        $this->quote = $quote;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->quote->video;
        $path = str_replace('public/videos/', '', $path);
        
        $destinationStorage = 'converted_videos';//'s3';
        $current_timestamp = Carbon::now()->timestamp;
        $destinatinPath = $current_timestamp."/".$this->quote->title.'.mp4';
        
        try {
            \FFMpeg::fromDisk('videos')
            ->open($path)
            ->export()
            ->toDisk($destinationStorage)
            ->inFormat(new \FFMpeg\Format\Video\X264('aac'))
            ->save($destinatinPath);
        
            // removing the original file
            Storage::disk($this->quote->disk)->delete($this->quote->video);
            // updating the record
            $this->quote->video = $destinatinPath;
            $this->quote->disk = $destinationStorage;
            $this->quote->save();

        }catch(RuntimeException $e) {

        }
    }
}
