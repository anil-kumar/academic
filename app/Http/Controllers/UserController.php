<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\UserProfile;
Use Image;
use Intervention\Image\Exception\NotReadableException;
use App\Http\Requests\Profile;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::with(['profile', 'qualification'])
            ->where(['id' => Auth::user()->id])
            ->first();
        return view('user.index', ['user' => $user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Profile $request, $id)
    {   //dd($request->input());
        $userData = $request->input();
        $settingData = $request->input('user_profile');
        $educationData = $request->input('education');
        unset($userData['user_profile']);
        unset($userData['education']);
        
        if($request->file('photo')) {
            $settingData['photo'] = $this->saveImage($request);
        }

        $user = User::where(['id' => $id])->first();
       
        $user->update($userData);        
        $user->profile()->updateOrCreate(['user_id' => $id], $settingData);
        $user->qualification()->updateOrCreate(['user_id' => $id], $educationData);
        return back()->with('success', "Profile has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function saveImage($request) {
        request()->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($files = $request->file('photo')) {
            $desinationFolder = 'public/avatar/'.$request->user()->id;
            // for save original image
            $path = $request->file('photo')
                ->storeAs($desinationFolder, $files->getClientOriginalName());

            // for save thumnail image
            $ImageUpload = Image::make($files);
            $thumbnailPath = storage_path().'/app/'.$desinationFolder.'/thumb_';

            // Creating thumbnail for image;
            $ImageUpload->resize(250,250);
            $ImageUpload = $ImageUpload->save($thumbnailPath.$files->getClientOriginalName());

            //updating image in profile table profile table
            return $files->getClientOriginalName();
        }
    }
}
