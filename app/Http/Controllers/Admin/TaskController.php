<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Task;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $tasks = Task::withTrashed()
            ->sortable()
            ->paginate(15);
        } catch (\Kyslik\ColumnSortable\Exceptions\ColumnSortableException $e) {
        }
        
        $users = \App\User::where(['role_id' => config('custom.roles.student')])->get();
        return view('admin.task.index', [
            'tasks' => $tasks,
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //creating package
        $data=$request->input();
        $data['user_id'] = Auth::user()->id;
        $package = Task::create($data);
        return redirect(route('admin.tasks.index'))
            ->with('success', 'Task has been created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $data=$request->input();
        $task->fill($data);
        $task->save();
        return redirect()->route('admin.tasks.index')
            ->with('success', "Task has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::withTrashed()->find($id);
        $message='deleted';
        if ($task->trashed()) {
            $task->restore();
            $message='restored';
        } else {
            $task->delete();
        }

        return back()
            ->with('success', "Task has been $message successfully.");
    }
}
