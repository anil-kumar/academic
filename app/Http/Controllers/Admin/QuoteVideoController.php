<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\QuoteVideo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Jobs\ProcessQuoteVideo;
use Illuminate\Support\Facades\Log;
use FFMpeg\Exception\RuntimeException;


class QuoteVideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $videos = QuoteVideo::withTrashed()
            ->sortable()
            ->paginate(15);
        } catch (\Kyslik\ColumnSortable\Exceptions\ColumnSortableException $e) {
        }
        return view('admin.quote.index', ['videos'=>$videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $data=$request->input();
        if ($files = $request->file('video')) { 
            $current_timestamp = Carbon::now()->timestamp;   
            $desinationFolder = $current_timestamp;            
            
            // $path = Storage::disk('s3')->put($desinationFolder, $request->file('video'));

            // for save original image
            $path = Storage::disk('videos')
                ->put($desinationFolder, $request->file('video'));
            $data['video'] = $path;
        }
        // saving record in table
        $package = QuoteVideo::create($data);
        // dispatching job to queue
        ProcessQuoteVideo::dispatch($package)
            ->delay(now()->addMinutes(1));

        return redirect(route('admin.quote-videos.index'))
            ->with('success', 'Package created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $data=$request->input();
        if ($files = $request->file('video')) {
            $current_timestamp = Carbon::now()->timestamp;   
            $desinationFolder = $current_timestamp;            
            
            // $path = Storage::disk('s3')->put($desinationFolder, $request->file('video'));

            // for save original image
            $path = Storage::disk('videos')
                ->put($desinationFolder, $request->file('video'));
            $data['video'] = $path;
        }
        // dd($data);
        $package = QuoteVideo::updateOrCreate(['id' => $id],$data);
        if ($files = $request->file('video')) {
            // dispatching job to queue
            ProcessQuoteVideo::dispatch($package)
                ->delay(now()->addMinutes(1));
        }

        return redirect()->route('admin.quote-videos.index')
            ->with('success', "Quote/Video has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = QuoteVideo::withTrashed()->find($id);
        $message='deleted';
        if ($task->trashed()) {
            $task->restore();
            $message='restored';
        } else {
            $task->delete();
        }

        return back()
            ->with('success', "Quote/Video has been $message successfully.");
    }
}
