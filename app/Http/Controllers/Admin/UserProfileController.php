<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
Use Image;
use App\User;
use App\UserProfile;
use Illuminate\Validation\Rule;
use Intervention\Image\Exception\NotReadableException;

class UserProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $user = User::where(['id' => Auth::user()->id])->first();
        return view('admin.user_profile.create', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   $id = Auth::user()->id;
        request()->validate([
            'name' => 'required',
            'user_profile.phone_no' => 'required',
            'email' => [
                'required', 'string', 
                'email', 
                'max:255', 
                Rule::unique('users')->ignore($id)
            ],
        ]);
        if($request->file('photo')) {
            $this->saveImage($request);
        }

        
        $userData = $request->input();
        $settingData = $request->input('user_profile');
        unset($userData['user_profile']);

        $user = User::where(['id' => $id])->first();
        $user->update($userData);        
        $user->profile()->updateOrCreate(['user_id' => $id], $settingData);

        return redirect()->route('admin.user-profiles.create')->with('success', "Profile has been updated successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function saveImage($request) {
        request()->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($files = $request->file('photo')) {
            $desinationFolder = 'public/avatar/'.$request->user()->id;
            // for save original image
            $path = $request->file('photo')
                ->storeAs($desinationFolder, $files->getClientOriginalName());

            // for save thumnail image
            $ImageUpload = Image::make($files);
            $thumbnailPath = storage_path().'/app/'.$desinationFolder.'/thumb_';

            // Creating thumbnail for image;
            $ImageUpload->resize(250,250);
            $ImageUpload = $ImageUpload->save($thumbnailPath.$files->getClientOriginalName());
            //updating image in profile table profile table
            $photo = UserProfile::firstOrNew([
                'user_id' => $request->user()->id
            ]);

            $photo->photo = $files->getClientOriginalName();
            $photo->save();
        }
    }
}
