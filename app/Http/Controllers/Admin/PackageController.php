<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Package;
use App\Http\Requests\StorePackage;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $packages = Package::with('availability')
            ->withTrashed()
            ->sortable()
            ->paginate(15);
        } catch (\Kyslik\ColumnSortable\Exceptions\ColumnSortableException $e) {
        }
        return view('admin.package.index', ['packages' => $packages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePackage $request)
    {
        //creating package
        $data= $request->input();
        $availability = $request->input('availability');
        unset($data['availability']);

        DB::transaction(function () use($data, $availability) {            
            $package = Package::create($data);
            $package->availability()->createMany($availability);
        });

        return redirect(route('admin.packages.index'))
            ->with('success', 'Package created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Package $package)
    {
        return view('admin.package.edit', ['package' => $package]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePackage $request, Package $package)
    {
        $data=$request->input();
        $availability = $request->input('availability');
        unset($data['availability']);

        DB::transaction(function () use($data, $availability, $package) {            
            $package->fill($data);
            $package->save();
            foreach ($availability as $key => $value) {
                $value['package_id'] = $package->id;
                $package->availability()
                    ->updateOrCreate([
                        'package_id' => $package->id,
                        'day' => $value['day']
                    ], $value);
            }
        });
        
        return redirect()->route('admin.packages.index')
            ->with('success', "Package has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Package::withTrashed()->find($id);
        $message='deleted';
        if ($package->trashed()) {
            $package->restore();
            $message='restored';
        } else {
            $package->delete();
        }

        return back()
            ->with('success', "Package has been $message successfully.");
    }
}
