<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Auth;
use App\User;
class ChangePasswordController extends Controller
{
	public function __construct()
    {
        $this->middleware(['verified']);
    }

    public function save(Request $request) {
		$request->validate($this->rules(), $this->validationErrorMessages());
		$id = Auth::user()->id;
		$password = $request->input('new_password');
		$user = User::where(['id' => $id])->first();
		$user->password = Hash::make($password);
		$user->save();
		return back()->with('success', "Password has been updated successfully.");
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
	protected function rules()
	{
		return [
			'password' => 'required|password:web|min:8',
			'new_password' => 'required|min:8',
			'password_confirmation' => 'required|same:new_password|min:8',
		];
	}

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
		return [
			'required' => 'The :attribute field is required',
			'min' => 'The :attribute should be atleast 8 character long',
			'same' => 'The :attribute not matched'
		];
    }
}
