<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Profile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => [
                'required',
                'string',
                'max:255',
                Rule::unique('users')->ignore($this->user()->id),
            ],
            'user_profile.parent_name' => 'required|string|max:255',
            'user_profile.parent_email' => 'required|string|email|max:255',
            'user_profile.phone_no' => 'required|string|max:15',
            'user_profile.address' => 'required|string|max:255',
            'user_profile.city' => 'required|string|max:20',
            'user_profile.state_zip' => 'required|string|max:20',
            'education.high_school_attending' => 'required|string|max:255',
            'education.other_high_school_attending' => 'required|string|max:255',
            'education.year_of_graduation' => 'required|string|max:255',
            'education.high_school_start_year' => 'required|string|max:255',
            'education.sport1' => 'required|string|max:255',
            'education.sport2' => 'required|string|max:255',
            'education.sport_club_team1' => 'required|string|max:255',
            'education.sport_club_team2' => 'required|string|max:255',
        ];
    }
}
