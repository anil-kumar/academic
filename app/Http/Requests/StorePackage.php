<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePackage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'no_of_session' => 'required|digits_between:1,10',
            'package_price' => 'required|numeric',
            'session_price' => 'required|numeric',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
        ];
    }
}
