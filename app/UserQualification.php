<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQualification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','high_school_attending', 'other_high_school_attending', 'year_of_graduation', 'high_school_start_year', 'iep_accommodation', 'sport1', 'sport2', 'sport_club_team1','sport_club_team2',
    ];
}
