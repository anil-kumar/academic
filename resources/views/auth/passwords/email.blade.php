@extends('layouts.login')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 tw-mt-16">
                <div class="tw-bg-white tw-rounded-lg tw-border-solid tw-border-2 tw-border-teal-100 tw-py-8 tw-px-5">

                    <h2 class="text-center tw-mt-0 tw-mb-6">
                        {{ __('Reset Password') }}
                    </h2>     
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group">
                            <label>Email Address</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="tw-flex tw-justify-between tw-items-center tw-mt-5">
                            <div>
                                <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
