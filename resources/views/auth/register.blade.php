@extends('layouts.login')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-12 col-lg-8 tw-mt-16">
            <div class="tw-bg-white tw-rounded-lg tw-border-solid tw-border-2 tw-border-teal-100 tw-py-8 tw-px-5">
                <h2 class="text-center tw-mt-0 tw-mb-6">
                    {{ __('Register Now') }}
                </h2>
                <form method="POST" action="{{ route('register') }}" class="tw-max-w-2xl mx-auto" id="RegisterForm">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="fullName">Student Full Name *</label>
                            <input type="text" class="form-control" id="fullName" name='name' value="{{old('name')}}" required>
                            @error('name')
                                <label for="fullName" id="fullName-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="email">Student Email Address *</label>
                            <input type="email" name="email" class="form-control" id="email"  value="{{old('email')}}" required>
                            @error('email')
                                <label for="email" id="email-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="guardianFullName">Parent/Guardian Full Name *</label>
                            <input type="text" class="form-control" id="guardianFullName" name="user_profile[parent_name]" value="{{ old('user_profile.parent_name') }}" >
                            @error('user_profile.parent_name')
                                <label for="guardianFullName" id="guardianFullName-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="guardianEmail">Parent/Guardian Email Address *</label>
                            <input type="email" class="form-control" id="guardianEmail" name="user_profile[parent_email]" value="{{old('user_profile.parent_email')}}" >
                            @error('user_profile.parent_email')
                                <label for="guardianEmail" id="guardianEmail-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="phone">Phone Number *</label>
                            <input type="tel" class="form-control" id="phone" name="phone" value="{{old('user_profile.phone_no')}}" >
                            @error('user_profile.phone_no')
                                <label for="phone" id="phone-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="streetAddress">Street Address *</label>
                            <input type="text" class="form-control" id="streetAddress" name="user_profile[address]" value="{{ old('user_profile.address') }}" >
                            @error('user_profile.address')
                                <label for="streetAddress" id="streetAddress-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="city">
                                City *
                            </label>
                            <input type="text" class="form-control" id="city" name="user_profile[city]" value="{{ old('user_profile.city') }}" required>
                            @error('user_profile.city')
                                <label for="city" id="city-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="zip">
                                State / Zip *
                            </label>
                            <input type="text" name="user_profile[state_zip]" class="form-control" id="state_zip" value="{{old('user_profile.state_zip')}}" required>
                            @error('user_profile.state_zip')
                                <label for="state_zip" id="state_zip-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="password"> 
                                Password *
                            </label>
                            <input type="password" class="form-control" id="password" name="password" required >
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="confirmPassword">Confirm Password *</label>
                            <input type="password" class="form-control" id="confirmPassword" name="password_confirmation" required>
                        </div>
                    </div>
                    <div class="row tw-mt-6 tw-justify-center">
                        <div class="form-group col-md-7 text-center">
                            <div class="form-check">
                                <label class="form-check-label" for="disabledFieldsetCheck">
                                    {{__('By Clicking "Sign Up" button, you agree with our')}}
                                    <a href="" class="tw-underline text-muted">{{__('Terms and conditions')}}</a> {{__('and')}}
                                    <a href="" class="tw-underline text-muted">{{__('Privacy Policy')}}</a>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row tw-mt-10">
                        <div class="form-group col-md-12 text-center">
                            <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">{{ __('Register Now')}}</button>
                        </div>
                    </div>
                    <p class="text-center tw-mt-2 tw-font-semibold">
                        {{__("Already have an account?")}}
                        <a href="{{ route('login') }}" class="tw-text-teal-100 hover:tw-text-teal-100">
                            {{__('Sign In')}}
                        </a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

<link rel="stylesheet" href="{{ asset('build/css/intlTelInput.css') }}"></link>
@push('scripts')
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery.timepicker.min.js') }}"></script>
    <script src="{{ asset('build/js/intlTelInput.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('js/user/register.js') }}"></script>
@endpush
