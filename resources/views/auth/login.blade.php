@extends('layouts.login')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 tw-mt-16">
                <div class="tw-bg-white tw-rounded-lg tw-border-solid tw-border-2 tw-border-teal-100 tw-py-8 tw-px-5">

                    <h2 class="text-center tw-mt-0 tw-mb-6">
                        {{ __('Login') }}
                    </h2>

                    <x-alert/>      
                    <form class="tw-max-w-sm mx-auto" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label class="tw-font-semibold" for="email">
                                {{ __('Email address') }}
                            </label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                            
                            <small id="emailHelp" class="form-text text-muted"> 
                                We'll never share your email with anyone else.
                            </small>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="tw-font-semibold" for="password"> 
                                {{__('Password')}}
                            </label>                            
                            
                            <input id="password" type="password" class="form-control" name="password" required > 
                            
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="tw-flex tw-justify-between tw-items-center tw-mt-5">
                            <div>
                                <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">
                                    {{ __('Sign In') }}
                                </button>
                            </div>
                            <a href="{{route('password.request')}}" class="tw-text-teal-100 tw-font-semibold hover:tw-text-teal-100">
                                {{ __('Forgot Password?') }}
                            </a>
                        </div>
                        <p class="text-center tw-mt-8 tw-font-semibold">
                            {{ __("Don't have an account?") }}
                            <a href="{{route('register')}}" class="tw-text-teal-100 hover:tw-text-teal-100">
                                {{ __('Sign Up') }}
                            </a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection