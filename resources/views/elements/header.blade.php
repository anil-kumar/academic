<div class="tw-bg-gray-200 fixed-top tw-pt-10">
    <nav class="navbar navbar-expand-md bg-white shadow-sm">
        <div class="container tw-relative">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="tw-absolute tw-top-0 tw--mt-10" src="{{ asset('images/academic-logo.png') }}" title="{{ config('app.name', 'Laravel') }}"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <svg class="tw-fill-current tw-text-black" viewBox="0 0 20 20" width="25px">
                    <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                        <g id="icon-shape">
                            <path d="M0,3 L20,3 L20,5 L0,5 L0,3 Z M0,9 L20,9 L20,11 L0,11 L0,9 Z M0,15 L20,15 L20,17 L0,17 L0,15 Z" id="Combined-Shape"></path>
                        </g>
                    </g>
                </svg>
            </button>
    
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto tw-py-3 text-right">
                    <!-- Authentication Links -->
                    @if (route('register') == url()->current())
                        <li class="nav-item tw-pr-3">
                            <a class="tw-block md:tw-inline-block tw-py-2 md:tw-py-0 md:tw-bg-teal-100 md:tw-rounded-full md:tw-px-8 md:tw-py-2 md:tw-text-white md:hover:tw-text-white hover:tw-no-underline" href="{{route('login')}}">{{ __('Login') }}</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="tw-block md:tw-inline-block tw-py-2 md:tw-py-0 md:tw-bg-teal-100 md:tw-rounded-full md:tw-px-8 md:tw-py-2 md:tw-text-white md:hover:tw-text-white hover:tw-no-underline" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</div>