<ul class="navbar-nav tw-bg-gray-800 tw-w-64 tw-min-h-screen tw-px-3 tw-flex-none sidebar">
    <!-- Sidebar - Brand -->
    <li>
        <a class="sidebar-brand d-flex align-items-center justify-content-center tw-py-6" href="index.html">
            <img src="{{ asset('images/academic-logo.png') }}" title="Academic"/>
        </a>
    </li>

    <!-- Nav Item - Dashboard -->
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-900 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="dashboard">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M11.9321237,9.48208981 C12.110309,10.1493234 11.9376722,10.8907549 11.4142136,11.4142136 C10.633165,12.1952621 9.36683502,12.1952621 8.58578644,11.4142136 C7.80473785,10.633165 7.80473785,9.36683502 8.58578644,8.58578644 C9.10924511,8.06232776 9.8506766,7.88969103 10.5179102,8.06787625 L13.5355339,5.05025253 L14.9497475,6.46446609 L11.9321237,9.48208981 Z M15.5995658,15.7135719 C17.0809102,14.261603 18,12.238134 18,10 C18,5.581722 14.418278,2 10,2 C5.581722,2 2,5.581722 2,10 C2,12.238134 2.91908983,14.261603 4.40043425,15.7135719 C5.99810554,14.63183 7.92526686,14 10,14 C12.0747331,14 14.0018945,14.63183 15.5995658,15.7135719 Z M10,20 C15.5228475,20 20,15.5228475 20,10 C20,4.4771525 15.5228475,0 10,0 C4.4771525,0 0,4.4771525 0,10 C0,15.5228475 4.4771525,20 10,20 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('Dashboard')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="index.html">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M11,11.7324356 C11.5978014,11.3866262 12,10.7402824 12,10 C12,8.8954305 11.1045695,8 10,8 C8.8954305,8 8,8.8954305 8,10 C8,10.7402824 8.40219863,11.3866262 9,11.7324356 L9,20 L11,20 L11,11.7324356 Z M14.2426407,14.2426407 C15.3284271,13.1568542 16,11.6568542 16,10 C16,6.6862915 13.3137085,4 10,4 C6.6862915,4 4,6.6862915 4,10 C4,11.6568542 4.67157288,13.1568542 5.75735931,14.2426407 L7.17157288,12.8284271 C6.44771525,12.1045695 6,11.1045695 6,10 C6,7.790861 7.790861,6 10,6 C12.209139,6 14,7.790861 14,10 C14,11.1045695 13.5522847,12.1045695 12.8284271,12.8284271 L14.2426407,14.2426407 L14.2426407,14.2426407 Z M17.0710678,17.0710678 C18.8807119,15.2614237 20,12.7614237 20,10 C20,4.4771525 15.5228475,0 10,0 C4.4771525,0 0,4.4771525 0,10 C0,12.7614237 1.11928813,15.2614237 2.92893219,17.0710678 L4.34314575,15.6568542 C2.8954305,14.209139 2,12.209139 2,10 C2,5.581722 5.581722,2 10,2 C14.418278,2 18,5.581722 18,10 C18,12.209139 17.1045695,14.209139 15.6568542,15.6568542 L17.0710678,17.0710678 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('College Tracking')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="tasks">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M14,14 L18,14 L18,2 L2,2 L2,14 L6,14 L6,14.0020869 C6,15.1017394 6.89458045,16 7.99810135,16 L12.0018986,16 C13.1132936,16 14,15.1055038 14,14.0020869 L14,14 Z M0,1.99079514 C0,0.891309342 0.898212381,0 1.99079514,0 L18.0092049,0 C19.1086907,0 20,0.898212381 20,1.99079514 L20,18.0092049 C20,19.1086907 19.1017876,20 18.0092049,20 L1.99079514,20 C0.891309342,20 0,19.1017876 0,18.0092049 L0,1.99079514 Z M5,9 L7,7 L9,9 L13,5 L15,7 L9,13 L5,9 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('Tasks')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="appointments">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M1,3.99508929 C1,2.8932319 1.8926228,2 2.99508929,2 L17.0049107,2 C18.1067681,2 19,2.8926228 19,3.99508929 L19,18.0049107 C19,19.1067681 18.1073772,20 17.0049107,20 L2.99508929,20 C1.8932319,20 1,19.1073772 1,18.0049107 L1,3.99508929 Z M3,6 L17,6 L17,18 L3,18 L3,6 Z M5,0 L7,0 L7,2 L5,2 L5,0 Z M13,0 L15,0 L15,2 L13,2 L13,0 Z M5,9 L7,9 L7,11 L5,11 L5,9 Z M5,13 L7,13 L7,15 L5,15 L5,13 Z M9,9 L11,9 L11,11 L9,11 L9,9 Z M9,13 L11,13 L11,15 L9,15 L9,13 Z M13,9 L15,9 L15,11 L13,11 L13,9 Z M13,13 L15,13 L15,15 L13,15 L13,13 Z" id="Combined-Shape"></path>							</g>
                </g>
            </svg>
            <span>{{__('Appointments')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="appointmentNotes">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M2,4 L2,18 L16,18 L16,12 L18,10 L18,20 L17,20 L0,20 L0,19 L0,3 L0,2 L10,2 L8,4 L2,4 Z M12.2928932,3.70710678 L4,12 L4,16 L8,16 L16.2928932,7.70710678 L12.2928932,3.70710678 Z M13.7071068,2.29289322 L16,0 L20,4 L17.7071068,6.29289322 L13.7071068,2.29289322 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('Appointments Notes')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="index.html">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M12.9728369,2.59456737 C12.7749064,1.12946324 11.5193533,0 10,0 C8.48064666,0 7.2250936,1.12946324 7.02716314,2.59456737 L5,3 L5,4 L3.99406028,4 C2.89451376,4 2,4.8927712 2,5.99406028 L2,18.0059397 C2,19.1054862 2.8927712,20 3.99406028,20 L16.0059397,20 C17.1054862,20 18,19.1072288 18,18.0059397 L18,5.99406028 C18,4.89451376 17.1072288,4 16.0059397,4 L15,4 L15,3 L12.9728369,2.59456737 Z M5,6 L4,6 L4,18 L16,18 L16,6 L15,6 L15,7 L5,7 L5,6 Z M10,4 C10.5522847,4 11,3.55228475 11,3 C11,2.44771525 10.5522847,2 10,2 C9.44771525,2 9,2.44771525 9,3 C9,3.55228475 9.44771525,4 10,4 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('Classroom')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="uploads">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M13,10 L13,16 L7,16 L7,10 L2,10 L10,2 L18,10 L13,10 Z M0,18 L20,18 L20,20 L0,20 L0,18 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('Uploads')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="packages">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M0,2 C0,0.8954305 0.898212381,0 1.99079514,0 L18.0092049,0 C19.1086907,0 20,0.887729645 20,2 L20,4 L0,4 L0,2 Z M1,5 L19,5 L19,18.0081158 C19,19.1082031 18.1073772,20 17.0049107,20 L2.99508929,20 C1.8932319,20 1,19.1066027 1,18.0081158 L1,5 Z M7,7 L13,7 L13,9 L7,9 L7,7 Z" id="Combined-Shape"></path>			
                    </g>
                </g>
            </svg>
            <span>{{__('All Packages')}}</span>
        </a>
    </li>
    <li>
        <a class="tw-px-4 tw-py-3 tw-text-white hover:tw-text-white tw-bg-gray-800 tw-rounded-md tw-block hover:tw-no-underline tw-flex tw-items-center" href="sessionVideo">
            <svg viewBox="0 0 20 20" class="tw-fill-current tw-mr-3 tw-text-white tw-h-5">
                <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                    <g id="icon-shape">
                        <path d="M0,3.99406028 C0,2.8927712 0.894513756,2 1.99406028,2 L14.0059397,2 C15.1072288,2 16,2.89451376 16,3.99406028 L16,16.0059397 C16,17.1072288 15.1054862,18 14.0059397,18 L1.99406028,18 C0.892771196,18 0,17.1054862 0,16.0059397 L0,3.99406028 Z M8,14 C10.209139,14 12,12.209139 12,10 C12,7.790861 10.209139,6 8,6 C5.790861,6 4,7.790861 4,10 C4,12.209139 5.790861,14 8,14 Z M8,12 C9.1045695,12 10,11.1045695 10,10 C10,8.8954305 9.1045695,8 8,8 C6.8954305,8 6,8.8954305 6,10 C6,11.1045695 6.8954305,12 8,12 Z M16,7 L20,3 L20,17 L16,13 L16,7 Z" id="Combined-Shape"></path>
                    </g>
                </g>
            </svg>
            <span>{{__('Recorded Sessions')}}</span>
        </a>
    </li>

</ul>