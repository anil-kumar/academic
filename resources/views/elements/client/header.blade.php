<nav class="navbar navbar-expand-lg navbar-light bg-light tw-shadow mb-4">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggle" type="button" class="btn lg:tw-hidden">
        <svg class="tw-fill-current tw-text-gray-600" viewBox="0 0 20 20" width="25px">
            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                <g id="icon-shape">
                    <path d="M0,3 L20,3 L20,5 L0,5 L0,3 Z M0,9 L20,9 L20,11 L0,11 L0,9 Z M0,15 L20,15 L20,17 L0,17 L0,15 Z" id="Combined-Shape"></path>
                </g>
            </g>
        </svg>
    </button>
    {{-- <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <svg class="tw-fill-current tw-text-black" viewBox="0 0 20 20" width="25px">
            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                <g id="icon-shape">
                    <path d="M0,3 L20,3 L20,5 L0,5 L0,3 Z M0,9 L20,9 L20,11 L0,11 L0,9 Z M0,15 L20,15 L20,17 L0,17 L0,15 Z" id="Combined-Shape"></path>
                </g>
            </g>
        </svg>
    </button> --}}

    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto" id="navbarSupportedContent">

        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 tw-font-semibold">
                    {{auth()->user()->name}}
                </span>
                <img class="img-profile rounded-circle tw-w-8" src="{{asset(auth()->user()->thumb_image)}}">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{ route('users.index') }}">
                    {{__('Profile')}}
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>

            </div>
        </li>
    </ul>
</nav>