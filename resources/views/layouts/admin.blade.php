<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ __('AcademicCourse Admin') }}</title>

        <link href="{{ asset('css/common.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
        <link href="{{ asset('css/front/base.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/favicon.ico">
        @stack('css')
    </head>
    <body> 
        <!-- Sidebar -->
        @yield('sidebar', View::make('admin.navs.sidebar'))
        <div class="page">
            <!-- Top Header -->
            @yield('header', View::make('admin.navs.header'))
            <!-- Content or view that extends this layout -->
            @yield('content')
        </div>
        @hasSection('modal')
            @yield('modal')
        @endif
        <script src="{{ asset('js/common.js') }}"></script>
        <script src="{{ asset('js/admin.js') }}"></script>
        @stack('scripts')
        <style type="text/css">
            .error {
                color:red;
            }
        </style>
    </body>
</html>