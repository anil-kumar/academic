<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="tw-bg-gray-200 text-muted h-100">
    
    <div id="app" class="d-flex flex-column h-100">
        {{-- include Header --}}
        @include('elements.header')

        <main class="py-4 flex-shrink-0 tw-my-24">            
            <!-- alert message component -->
            <x-alert/>      
            @yield('content')
        </main>

        {{-- include Footer --}}
        @include('elements.footer')
    </div>

    @hasSection('modal')
        @yield('modal')
    @endif
    
    <script src="{{ asset('js/common.js') }}"></script> 
    @stack('scripts')
    <style type="text/css">
        .error {
            color:red;
        }
        .iti {
            width: 100%
        }
    </style>
</body>
</html>
