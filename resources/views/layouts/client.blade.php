<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="tw-relative tw-min-h-full">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="tw-bg-gray-200 text-muted tw-h-screen lg:tw-h-full tw-font-sans tw-text-base">
    <div id="app" class="tw-flex tw-h-full">
        
        {{-- include sidebar --}}
        @include('elements/client/sidebar')

        <main class="d-flex flex-column tw-w-full">

            <div class="tw-flex-1">
                {{-- include client header --}}
                @include('elements/client/header')
                
                @yield('content')
            </div>

            {{-- include Footer --}}
            @include('elements/client/footer')

        </main>

        {{-- Modal: Book Appointment --}}
        @include('modals/client/bookAppointment')

        {{-- Modal: Add New Task --}}
        @include('modals/client/addNewTask')

        {{-- Modal: Change Password --}}
        @include('modals/client/changePassword')

    </div>

    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{ asset('js/client.js') }}"></script>

    @stack('scripts')
    <style type="text/css">
        .error {
            color:red;
        }
        .iti {
            width: 100%
        }
    </style>
    
</body>
</html>
