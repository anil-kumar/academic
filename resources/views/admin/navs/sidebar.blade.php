<nav class="side-navbar">
    <div class="side-navbar-wrapper">
        <!-- Sidebar Header    -->
        <div class="sidenav-header d-flex align-items-center justify-content-center">
            <!-- User Info-->
            <div class="sidenav-header-inner text-center">
                <!-- <img src="img/avatar-7.jpg" alt="person" class="img-fluid rounded-circle"> -->
                <h2 class="h5">Welcome</h2><span>{{auth()->user()->full_name}}</span>
            </div>
            <!-- Small Brand information, appears on minimized sidebar-->
            <div class="sidenav-header-logo">
                <a href="index.html" class="brand-small text-center"> 
                    {{auth()->user()->full_name}}
                </a>
            </div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
            <ul id="side-main-menu" class="side-menu list-unstyled">                  
                <li class="{{ route('admin.dashboard') == url()->current() ? 'active': null }}">
                    <a href="#">
                        <i class="fa fa-dashboard"></i>{{ __('Dashboard') }}
                    </a>
                </li>
                <li class="{{ route('admin.users.index') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="fa fa-group"></i>{{ __('Users') }}
                    </a>
                </li>
                <li class="{{ route('admin.packages.index') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.packages.index') }}">
                        <i class="fa fa-cubes"></i>{{ __('Packages') }}
                    </a>
                </li> 
                <li class="{{ route('admin.tasks.index') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.tasks.index') }}">
                        <i class="fa fa-list"></i>{{ __('Tasks') }}
                    </a>
                </li> 
                <li class="{{ route('admin.quote-videos.index') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.quote-videos.index') }}">
                        <i class="fa fa-film"></i>{{ __('Quote/Videos') }}
                    </a>
                </li> 
                <li class="{{ route('admin.appointments.index') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.appointments.index') }}">
                        <i class="fa fa-address-book"></i>{{ __('Appointments') }}
                    </a>
                </li>
                <li class="{{ route('admin.user-forms.create') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.user-forms.create') }}">
                        <i class="fa fa-wpforms"></i>{{ __('Assign Forms') }}
                    </a>
                </li> 
                <li class="{{ route('admin.password.change') == url()->current() ? 'active': null }}">
                    <a href="{{ route('admin.password.change') }}">
                        <i class="fa fa-key"></i>{{ __('Change Password') }}
                    </a>
                </li>            
                <!-- <li>
                    <a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse">
                        <i class="icon-interface-windows"></i>Example dropdown
                    </a>
                    <ul id="exampledropdownDropdown" class="collapse list-unstyled ">
                        <li><a href="#">Page</a></li>
                        <li><a href="#">Page</a></li>
                        <li><a href="#">Page</a></li>
                    </ul>
                </li> -->
            </ul>
        </div>
    </div>
</nav>