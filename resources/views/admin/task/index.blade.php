@extends('layouts.admin')

@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-6">
            			<h2>Tasks</h2>
          			</div>
          			<div class="col-6 text-right">
          				<a href="javascript:void(0)" class="btn btn-default" data-toggle="modal" data-target="#assignTaskModal">
						    Assign Task
					  	</a>
          				<a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#addTaskModal">
						    Add Task
					  	</a>
          			</div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>			
          	</header>
          	<div class="row">
            	<div class="col">
              		<div class="card">
		                <div class="card-body">
		                  	<div class="table-responsive">
		                    	<table class="table">
			                      	<thead>
				                        <tr>
				                          	<th>#</th>
				                          	<th>
				                          		@sortablelink('name', __(' Name'), ['page' => $tasks->currentPage()])
				                          	</th>
				                          	<th>
			                          			@sortablelink('created_at', __('Created at'))
				                          	</th>
				                          	<th>Created By</th>
				                          	<th>
				                          		{{ __('Action') }}
				                          	</th>
				                        </tr>
			                      	</thead>
			                      	<tbody>
			                      		@if (count($tasks))
										    @foreach ($tasks as $task)
						                        <tr>
						                          	<th scope="row">{{$loop->index+1}}</th>
						                          	<td>{{ $task->name }}</td>
						                          	<td>
						                          		{{ $task->created_at->isoFormat(env('DATE_FORMAT')) }}
						                          	</td>
						                          	<td>
						                          		{{ ucwords($task->user->name) }}
						                          	</td>
						                          	<td>
					                          			<i class="fa fa-edit" data-obj="<?= base64_encode(json_encode($task)) ?>" data-url="{{ route('admin.tasks.update', ['task' => $task->hash_id]) }}"></i>
						                          		@if(!$task->deleted_at)
						                          			<i class="fa fa-trash" data-url="{{ route('admin.tasks.destroy', ['task' => $task->hash_id]) }}" data-message= 'Delete'>
						                          		@else
						                          			<i class="fa fa-undo" data-url="{{ route('admin.tasks.destroy', ['task' => $task->hash_id]) }}" data-message= 'Restore'>
						                          		@endif
						                          		</i>
						                          	</td>
						                        </tr>
										    @endforeach
										@else
										    <tr> <td colspan=4> {{__('No Record Found.') }}</td></tr>
										@endif
			                      		
			                      	</tbody>
			                    </table>
			                    <div class="text-right">
			                    	{!! $tasks->appends(\Request::except('page'))->render() !!}
			                    </div>
		                  	</div>
		                </div>
	              	</div>
	            </div>
          	</div>
        </div>
  	</section>
  	<form method="POST" action="" id="DeletePackageForm">
  		@method('DELETE')
        @csrf
  		
  	</form>
@endsection

@section('modal')
    @include('modals.admin.task.add')
    @include('modals.admin.task.edit')
    @include('modals.admin.task.assign')
@endsection

@push('css')
	<link rel="stylesheet" href="{{ asset('css/common/jquery-ui.css') }}"></link>
@endpush

@push('scripts')
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('js/common/jquery-ui.js')}}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/task/add.js') }}"></script>
@endpush