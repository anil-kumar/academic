@extends('layouts.admin')
@section('content')
	<section>
	    <div class="container-fluid">
	      	<!-- Page Header-->
	      	<header> 
	      		<div class="row">
	      			<div class="col-6">
	        			<h2>Edit Package</h2>
	      			</div>
	      		</div>				
	      	</header>
	      	<div class="row">
	        	<div class="col-md-8 mx-auto">
	          		<div class="card">
		                <div class="card-body">
		                  	<form method="POST" action="{{ route('admin.packages.update', ['package' => $package->hash_id]) }}" id="PackageForm">
		                  		@method('PUT')
	                            @csrf
	                            <div class="form-group">
	                                <label for="PackageName">{{ __('Package Name') }}</label>
	                                <input name="name" class="form-control" id="PackageName" value="{{ old('name', $package->name) }}" required> 
	                                @error('name')
	                                    <label class="">{{ $message }}</label>
	                                @enderror
	                            </div>
	                            <div class="form-group">
	                                <label for="PackageDescription">{{ __('Package Description') }}</label>
	                                <textarea name="description" class="form-control" id="PackageDescription" rows="2" required >{{ old('description', $package->description) }}</textarea>
	                                @error('description')
	                                    <label class="">{{ $message }}</label>
	                                @enderror
	                            </div>
	                            <div class="row">
	                            	<div class="col-md-6">
			                            <div class="form-group">
			                                <label for="PackageSession">{{ __('How many session included in this package') }}</label>

			                                <select name='no_of_session' class="form-control" id="PackageSession">
			                                	<option value="1">1</option>
			                                	<option value="2">2</option>
			                                	<option value="3">3</option>
			                                	<option value="4">4</option>
			                                	<option value="5">5</option>
			                                	<option value="6">6</option>
			                                	<option value="7">7</option>
			                                	<option value="8">8</option>
			                                	<option value="9">9</option>
			                                	<option value="10">10</option>
			                                </select>
			                                @error('no_of_package')
			                                    <label class="">{{ $message }}</label>
			                                @enderror
			                            </div>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="form-group">
			                                <label for="SessionDuration">
			                                	{{ __('Each session duration') }}
			                                </label>
			                                <input name="session_duration" class="form-control time" id="SessionDuration" value="{{ old('session_duration', $package->session_duration) }}"required> 
			                                @error('session_duration')
			                                    <label class="">{{ $message }}</label>
			                                @enderror
			                            </div>
	                            	</div>
	                            </div>
	                            <div class="row">
								    <div class="col-md-6">
			                            <div class="form-group">
			                                <label for="PackagePrice">{{ __('Package Price') }}</label>

			                                <input type="number" name="package_price" class="form-control" id="PackagePrice" value="{{ old('package_price', $package->package_price) }}" required> 
			                                @error('package_price')
			                                    <label class="">{{ $message }}</label>
			                                @enderror
			                            </div>
								    </div>
								    <div class="col-md-6">
			                            <div class="form-group">
			                                <label for="SessionPrice">{{ __('Individual session price') }}</label>
			                                <input type="number" name="session_price" class="form-control" id="SessionPrice" value="{{ old('session_price', $package->session_price) }}" required> 
			                                @error('package_price')
			                                    <label class="">{{ $message }}</label>
			                                @enderror
			                            </div>								      
								    </div>
							  	</div>
							  	
							  	<div class="my-4">
							  		<h3>Availability</h3>
							  	</div>

	                            <div class="row mb-4">
								    <div class="col-md-6">
			                            <div class="form-group">
			                                <label for="PackageFrom">{{ __('From') }}</label>

			                                <input name="date_from" class="form-control" id="PackageFrom" value="{{ old('date_from', $package->date_from) }}" required> 
			                                @error('date_from')
			                                    <label class="">{{ $message }}</label>
			                                @enderror
			                            </div>
								    </div>
								    <div class="col-md-6">
			                            <div class="form-group">
			                                <label for="PackageTo">{{ __('To') }}</label>
			                                <input name="date_to" class="form-control" id="PackageTo" value="{{ old('date_to', $package->date_to) }}" required> 
			                                @error('date_to')
			                                    <label class="">{{ $message }}</label>
			                                @enderror
			                            </div>								      
								    </div>
							  	</div>

							  	<?php 
							  		$dowMap = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'); 
							  		$availability = $package->availability->toArray();
							  	?>
							  	@for ($i = 0; $i < 7; $i++)
		                            <div class="row" id="datepairExample{{$i}}">
		                            	<div class="col-md-2">
		                            		<div class="form-group">
		                            			<button type="button" class="btn btn-success align-middle" style="min-width: 54px;">{{$dowMap[$i]}}</button>
		                            			<input type="hidden" name="availability[{{$i}}][day]" value='{{$i}}'>
		                            		</div>
		                            	</div>
									    <div class="col-md-5">
				                            <div class="form-group">
				                                <input name="availability[{{$i}}][time_from]" class="form-control time" required value="{{$availability[$i]['time_from']}}"> 
				                                @error('availability[time_from]')
				                                    <label class="">{{ $message }}</label>
				                                @enderror
				                            </div>
									    </div>
									    <div class="col-md-5">
				                            <div class="form-group">
				                                <input name="availability[{{$i}}][time_to]" class="form-control time" value="{{$availability[$i]['time_to']}}" required> 
				                                @error('date_to')
				                                    <label class="">{{ $message }}</label>
				                                @enderror
				                            </div>								      
									    </div>
								  	</div>
								@endfor

	                            <div class="form-group text-right">
	                            	<a href="{{route('admin.packages.index')}}" class="btn btn-default">Cancel</a>
	                            	<button type="submit" class="btn btn-success">Submit</button>
	                            </div>
	                        </form>
		                </div>
	              	</div>
	            </div>
	      	</div>
	    </div>
	</section>
@endsection
@push('css')
	<link rel="stylesheet" href="{{ asset('css/common/jquery-ui.css') }}"></link>
	<link rel="stylesheet" href="{{ asset('css/common/jquery.timepicker.min.css') }}"></link>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{asset('js/common/jquery-ui.js')}}"></script>
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery.timepicker.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('js/admin/package/create.js') }}"></script>
@endpush
