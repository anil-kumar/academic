@extends('layouts.admin')

@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-6">
            			<h2>Packages</h2>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{route('admin.packages.create')}}" class="btn btn-success">
						    Add Package
					  	</a>
          			</div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>			
          	</header>
          	<div class="row">
            	<div class="col">
              		<div class="card">
		                <div class="card-body">
		                  	<div class="table-responsive">
		                    	<table class="table">
			                      	<thead>
				                        <tr>
				                          	<th>#</th>
				                          	<th>
				                          		@sortablelink('name', __(' Name'), ['page' => $packages->currentPage()])
				                          	</th>
				                          	<th>				                          		
			                          			@sortablelink('package_price', __('Price'))
				                          	</th>
				                          	<th>
			                          			@sortablelink('created_at', __('Created at'))
				                          	</th>
				                          	<th>
				                          		{{ __('Action') }}
				                          	</th>
				                        </tr>
			                      	</thead>
			                      	<tbody>
			                      		@if (count($packages))
										    @foreach ($packages as $package)
						                        <tr>
						                          	<th scope="row">{{$loop->index+1}}</th>
						                          	<td>{{ $package->name }}</td>
						                          	<td>{{ $package->package_price }}</td>
						                          	<td>
						                          		{{ $package->created_at->isoFormat(env('DATE_FORMAT')) }}
						                          	</td>
						                          	<td>
						                          		<!-- <i class="fa fa-eye"></i> -->
						                          		<a href="{{ route('admin.packages.edit', ['package' => $package->hash_id]) }}">
						                          			<i class="fa fa-edit"></i>
						                          		</a>
						                          		@if(!$package->deleted_at)
						                          			<i class="fa fa-trash" data-url="{{ route('admin.packages.destroy', ['package' => $package->hash_id]) }}" data-message= 'Delete'>
						                          		@else
						                          			<i class="fa fa-undo" data-url="{{ route('admin.packages.destroy', ['package' => $package->hash_id]) }}" data-message= 'Restore'>
						                          		@endif
						                          		</i>
						                          	</td>
						                        </tr>
										    @endforeach
										@else
										    <tr> <td colspan=4> {{__('No Record Found.') }}</td></tr>
										@endif
			                      		
			                      	</tbody>
			                    </table>
			                    <div class="text-right">
			                    	{!! $packages->appends(\Request::except('page'))->render() !!}
			                    </div>
		                  	</div>
		                </div>
	              	</div>
	            </div>
          	</div>
        </div>
  	</section>
  	<form method="POST" action="" id="DeletePackageForm">
  		@method('DELETE')
        @csrf
  		
  	</form>
@endsection
@push('scripts')
	<script type="text/javascript">
		$('.fa-trash, .fa-undo').on('click', function(){
			let url = $(this).attr('data-url');
			let msg = $(this).attr('data-message');
			let form = $('#DeletePackageForm');
			if(confirm(`Are you sure want to ${msg} the package`)) {
				form.attr('action', url);
				form.submit();
			}
		});
	</script>
@endpush