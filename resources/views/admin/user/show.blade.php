@extends('layouts.admin')

@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
            	<h2 class="h3 display">{{ ucwords($user->name) }}</h2>
            	@if (session('status'))
				    <div class="alert alert-warning alert-dismissible fade show" role="alert">
				        {{ session('status') }}
				        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
					  	</button>
				    </div>
				@endif
          	</header>
          	<div class="row">
            	<div class="col">
            		<div class="card">
		                <div class="card-body">
		                    <div class="row">
		                        <div class="col-12 col-lg-8 col-md-6">
		                            <h3 class="mb-0 text-truncated">{{ ucwords($user->name) }}</h3>
		                            <p class="lead">Member Since:{{ $user->created_at->isoFormat(env('TIME_FORMAT')) }}</p>
		                            <p>
		                                Email: {{ $user->email }}
		                            </p>
		                            <p> 
		                            	<!-- <span class="badge badge-info tags">html5</span> 
		                                <span class="badge badge-info tags">css3</span>
		                                <span class="badge badge-info tags">nodejs</span> -->
		                            </p>
		                        </div>
		                        <div class="col-12 col-lg-4 col-md-6 text-center">
		                            <img src="{{ (!empty($user->userProfileSetting))?asset($user->userProfileSetting->thumb_image):asset('images/app/user.png') }}" alt="" class="mx-auto img-thumbnail rounded-circle img-fluid">
		                            <br>
		                            <!-- <ul class="list-inline ratings text-center" title="Ratings">
		                                <li class="list-inline-item"><a href="#"><span class="fa fa-star"></span></a>
		                                </li>
		                                <li class="list-inline-item"><a href="#"><span class="fa fa-star"></span></a>
		                                </li>
		                                <li class="list-inline-item"><a href="#"><span class="fa fa-star"></span></a>
		                                </li>
		                                <li class="list-inline-item"><a href="#"><span class="fa fa-star"></span></a>
		                                </li>
		                                <li class="list-inline-item"><a href="#"><span class="fa fa-star"></span></a>
		                                </li>
		                            </ul> -->
		                        </div>
		                        <div class="col-12">
		                            <h3 class="mb-2">User Goals</h3>
		                            <div class="table-responsive">
				                    	<table class="table">
					                      	<thead>
						                        <tr>
						                          	<th>#</th>
						                          	<th>Title</th>
						                          	<th>Priority</th>
						                          	<th>Description</th>
						                          	<th>Created at</th>
						                          	<th>
						                          		{{ __('Ambitions') }}
						                          	</th>
						                        </tr>
					                      	</thead>
					                      	<tbody>
					                      		@if ($user->goals->count())
												    @foreach ($user->goals as $goal)								        
								                        <tr>
								                          	<th scope="row"> {{ $loop->index+1 }} </th>
								                          	<td>
								                          		{{ $goal->title }}
								                          	</td>
								                          	<td>
								                          		{{ ucwords($goal->priority->name) }}
								                          	</td>
								                          	<td>
								                          		{{ $goal->description }}
								                          	</td>
								                          	<td>
								                          		{{ $goal->created_at->isoFormat(env('TIME_FORMAT')) }}
								                          	</td>
								                          	<td>						                          		
																<a class="dropdown-item" href="{{ route('admin.goals.show', [ 'goal'=> $goal->hash_id ]) }}" >
							                          				<i class="fa fa-eye"></i> {{ count($goal->ambitions) }}
							                          			</a>
								                          	</td>
								                        </tr>
												    @endforeach
												@else
												    <tr> <td colspan=4> {{__('No Goals Found.') }}</td></tr>
												@endif
					                      		
					                      	</tbody>
					                    </table>
				                  	</div>
		                        </div>
		                    </div>
		                    <!--/row-->
		                </div>
		                <!--/card-block-->
		            </div>
	            </div>
          	</div>
        </div>
  	</section>
@endsection
