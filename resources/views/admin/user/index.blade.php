@extends('layouts.admin')

@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
            	<h2 class="h3 display">Users</h2>            	
          		<!-- alert message component -->
            	<x-alert/>	
          	</header>
          	<div class="row">
            	<div class="col">
              		<div class="card">
		                <div class="card-body">
		                  	<div class="table-responsive">
		                    	<table class="table">
			                      	<thead>
				                        <tr>
				                          	<th>#</th>
				                          	<th>
				                          		@sortablelink('name', __('User Name'), ['page' => $users->currentPage()])
				                          	</th>
				                          	<th>				                          		
			                          			@sortablelink('email', __('Email'))
				                          	</th>
				                          	<th> @sortablelink('deleted_at', __('Deleted')) </th>
				                          	<th>
			                          			@sortablelink('created_at', __('Created at'))
				                          	</th>
				                          	<th>
				                          		{{ __('Action') }}
				                          	</th>
				                        </tr>
			                      	</thead>
			                      	<tbody>
			                      		@if (count($users))
										    @foreach ($users as $user)								        
						                        <tr>
						                          	<th scope="row">{{$loop->index+1}}</th>
						                          	<td>{{ $user->name }}</td>
						                          	<td>{{ $user->email }}</td>
						                          	<td>{{ ($user->deleted_at)?'Yes':'No' }}</td>
						                          	<td>{{ $user->created_at }}</td>
						                          	<td>						                          		
														<div class="dropdown">
															<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
															{{ __('Action') }}
															</button>
															<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																@if(!$user->deleted_at)
									                          		<form action="{{ action('Admin\UserController@destroy',[ 'user' => $user->id ]) }}" method="POST">
																	  	{{ csrf_field() }}
																	  	{{ method_field('DELETE') }} 
									                          			<a class="dropdown-item" href="#" class="" onclick="if(confirm('Are you sure to delete')){ this.closest('form').submit() }">
									                          				<i class="fa fa-trash"></i> Delete
									                          			</a>
																	</form>
																@else
									                          		<form action="{{ action('Admin\UserController@destroy',[ 'user' => $user->id ]) }}" method="POST">
																	  	{{ csrf_field() }}
																	  	{{ method_field('DELETE') }} 
									                          			<a class="dropdown-item" href="#" onclick="if(confirm('Are you sure to restore')){ this.closest('form').submit() }">
									                          				<i class="fa fa-undo text-danger"></i>
									                          			</a>
																	</form>
																@endif
																<a class="dropdown-item" href="{{ route('admin.users.show', [ 'user'=> $user->id ]) }}" >
							                          				<i class="fa fa-eye"></i> View
							                          			</a>
															</div>
														</div>
						                          	</td>
						                        </tr>
										    @endforeach
										@else
										    <tr> <td colspan=4> {{__('No Record Found.') }}</td></tr>
										@endif
			                      		
			                      	</tbody>
			                    </table>
			                    <div class="text-right">
			                    	{!! $users->appends(\Request::except('page'))->render() !!}
			                    </div>
		                  	</div>
		                </div>
	              	</div>
	            </div>
          	</div>
        </div>
  	</section>
@endsection
