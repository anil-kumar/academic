@extends('layouts.admin')
@section('content')
	<section>
	    <div class="container-fluid">
	      	<!-- Page Header-->
	      	<header> 
	      		<div class="row">
	      			<div class="col-6">
	        			<h2>Assign Forms</h2>
	      			</div>
	      		</div>
          		<!-- alert message component -->
            	<x-alert/>					
	      	</header>
	      	<div class="row">
	        	<div class="col-md-7 mx-auto">
	          		<div class="card">
		                <div class="card-body">
		                  	<form method="POST" action="{{ route('admin.user-forms.store') }}" id="PackageForm">
	                            @csrf
	                            <div class="form-group">
	                                <label for="FormName">{{ __('Select Form') }}</label>
	                                <select name="form_id" class="form-control" id="FormName" value="{{ old('form_id') }}" required>
	                                	@foreach($forms as $form)
		                                	<option value="{{$form->id}}">
		                                		{{$form->title}}
		                                	</option> 
	                                	@endforeach
	                                </select>
	                                @error('name')
	                                    <label class="">{{ $message }}</label>
	                                @enderror
	                            </div>
	                            <div class="form-group">
	                                <label for="UserName">{{ __('Select client which you want to send the form') }}</label>
	                                <select name="user_id" class="form-control" id="UserName" rows="2" required >
	                                	@foreach($users as $user)
		                                	<option value="{{$user->id}}">
		                                		{{$user->full_name}}
		                                	</option> 
	                                	@endforeach
	                                </select>
	                                @error('user_id')
	                                    <label class="">{{ $message }}</label>
	                                @enderror
	                            </div>

	                            <div class="form-group text-right">
	                            	<a href="{{route('admin.packages.index')}}" class="btn btn-default">Cancel</a>
	                            	<button type="submit" class="btn btn-success">Submit</button>
	                            </div>
	                        </form>
		                </div>
	              	</div>
	            </div>
	      	</div>
	    </div>
	</section>
@endsection
@push('css')
	<link rel="stylesheet" href="{{ asset('css/common/jquery-ui.css') }}"></link>
	<link rel="stylesheet" href="{{ asset('css/common/jquery.timepicker.min.css') }}"></link>
@endpush
@push('scripts')
    <script type="text/javascript" src="{{asset('js/common/jquery-ui.js')}}"></script>
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery.timepicker.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/package/create.js') }}"></script>
@endpush