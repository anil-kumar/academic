@extends('layouts.admin')

@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-md-8">
            			<h2>Appointments</h2>
          			</div>
          			<div class="col-md-4 text-right">
          				<form>
          					<div class="form-group">
          						<select name="filter" class="form-control" onchange="this.closest('form').submit()">
          							<option value="">All Appointment</option>
          							<option value="complete" {{($filter == "complete")?'selected':'null'}}>Complete Appointment</option>
          							<option value="upcoming" {{($filter == "upcoming")?'selected':'null'}}>Upcoming Appointment</option>
          						</select>
          					</div>
          				</form>
          			</div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>			
          	</header>
          	<div class="row">
            	<div class="col">
              		<div class="card">
		                <div class="card-body">
		                  	<div class="table-responsive">
		                    	<table class="table">
			                      	<thead>
				                        <tr>
				                          	<th>#</th>
				                          	<th>
				                          		@sortablelink('user_id', __('Client name'), ['page' => $appointments->currentPage()])
				                          	</th>
				                          	<th>
				                          		@sortablelink('package_id', __('Package name'))
				                          	</th>
				                          	<th>
			                          			@sortablelink('appointment_date', __('Appointment date/time'))
				                          	</th>
				                          	<th>
				                          		{{ __('Status') }}
				                          	</th>
				                        </tr>
			                      	</thead>
			                      	<tbody>
			                      		@if (count($appointments))
										    @foreach ($appointments as $appointment)
						                        <tr>
						                          	<th scope="row">{{$loop->index+1}}</th>
						                          	<td>{{ $appointment->user->name }}</td>
						                          	<td>{{ $appointment->package->name }} </td>
						                          	<td>
						                          		{{ $appointment->appointment_date->isoFormat(env('DATE_FORMAT')) }}
						                          		{{ $appointment->appointment_time }}
						                          	</td>
						                          	<td>
						                          		<span class="{{($appointment->status == 'complete')?'text-success':'text-blue'}}">
					                          				{{$appointment->status}}
						                          		</span>
						                          	</td>
						                        </tr>
										    @endforeach
										@else
										    <tr> <td colspan=4> {{__('No Record Found.') }}</td></tr>
										@endif
			                      		
			                      	</tbody>
			                    </table>
			                    <div class="text-right">
			                    	{!! $appointments->appends(\Request::except('page'))->render() !!}
			                    </div>
		                  	</div>
		                </div>
	              	</div>
	            </div>
          	</div>
        </div>
  	</section>
  	<form method="POST" action="" id="DeletePackageForm">
  		@method('DELETE')
        @csrf
  		
  	</form>
@endsection

@section('modal')
    @include('modals.admin.quote.create')
    @include('modals.admin.quote.edit')
@endsection
@push('scripts')
	<script type="text/javascript">
		$('.fa-trash, .fa-undo').on('click', function(){
			let url = $(this).attr('data-url');
			let msg = $(this).attr('data-message');
			let form = $('#DeletePackageForm');
			if(confirm(`Are you sure want to ${msg} the Quote/appointment`)) {
				form.attr('action', url);
				form.submit();
			}
		});
	</script>
@endpush

@push('css')
	<link rel="stylesheet" href="{{ asset('css/common/jquery.datetimepicker.min.css') }}"></link>
@endpush
@push('scripts')
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery.datetimepicker.full.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/appointment/create.js') }}"></script>
@endpush