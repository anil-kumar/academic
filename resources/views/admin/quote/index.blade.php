@extends('layouts.admin')

@section('content')
  	<section>
        <div class="container-fluid">
          	<!-- Page Header-->
          	<header> 
          		<div class="row">
          			<div class="col-6">
            			<h2>Quote/Videos</h2>
          			</div>
          			<div class="col-6 text-right">
          				<a href="{{route('admin.quote-videos.create')}}" class="btn btn-primary" data-toggle="modal" data-target="#addQuoteModal">
						    Add Quote/Video
					  	</a>
          			</div>
          		</div>
          		<!-- alert message component -->
            	<x-alert/>			
          	</header>
          	<div class="row">
            	<div class="col">
              		<div class="card">
		                <div class="card-body">
		                  	<div class="table-responsive">
		                    	<table class="table">
			                      	<thead>
				                        <tr>
				                          	<th>#</th>
				                          	<th>
				                          		@sortablelink('quote', __('Quote'), ['page' => $videos->currentPage()])
				                          	</th>
				                          	<th>
				                          		@sortablelink('title', __('Video'))
				                          	</th>
				                          	<th>
			                          			@sortablelink('display_at', __('Display at'))
				                          	</th>
				                          	<th>
				                          		{{ __('Action') }}
				                          	</th>
				                        </tr>
			                      	</thead>
			                      	<tbody>
			                      		@if (count($videos))
										    @foreach ($videos as $video)
						                        <tr>
						                          	<th scope="row">{{$loop->index+1}}</th>
						                          	<td>{{ $video->quote }}</td>
						                          	<td>{{ $video->title }}</td>
						                          	<td>
						                          		{{ $video->created_at }}
						                          	</td>
						                          	<td>
					                          			<i class="fa fa-edit" data-obj="<?= base64_encode(json_encode($video)) ?>" data-url="{{ route('admin.quote-videos.update', ['quote_video' => $video->hash_id]) }}"></i>
						                          		@if(!$video->deleted_at)
						                          			<i class="fa fa-trash" data-url="{{ route('admin.quote-videos.destroy', ['quote_video' => $video->hash_id]) }}" data-message= 'Delete'>
						                          		@else
						                          			<i class="fa fa-undo" data-url="{{ route('admin.quote-videos.destroy', ['quote_video' => $video->hash_id]) }}" data-message= 'Restore'>
						                          		@endif
						                          		</i>
						                          	</td>
						                        </tr>
										    @endforeach
										@else
										    <tr> <td colspan=4> {{__('No Record Found.') }}</td></tr>
										@endif
			                      		
			                      	</tbody>
			                    </table>
			                    <div class="text-right">
			                    	{!! $videos->appends(\Request::except('page'))->render() !!}
			                    </div>
		                  	</div>
		                </div>
	              	</div>
	            </div>
          	</div>
        </div>
  	</section>
  	<form method="POST" action="" id="DeletePackageForm">
  		@method('DELETE')
        @csrf
  		
  	</form>
@endsection

@section('modal')
    @include('modals.admin.quote.create')
    @include('modals.admin.quote.edit')
@endsection
@push('scripts')
	<script type="text/javascript">
		$('.fa-trash, .fa-undo').on('click', function(){
			let url = $(this).attr('data-url');
			let msg = $(this).attr('data-message');
			let form = $('#DeletePackageForm');
			if(confirm(`Are you sure want to ${msg} the Quote/Video`)) {
				form.attr('action', url);
				form.submit();
			}
		});
	</script>
@endpush

@push('css')
	<link rel="stylesheet" href="{{ asset('css/common/jquery.datetimepicker.min.css') }}"></link>
@endpush
@push('scripts')
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery.datetimepicker.full.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/admin/quote/create.js') }}"></script>
@endpush