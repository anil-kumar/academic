@extends('layouts.client')
@section('content')
<div class="container-fluid tw-px-6">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 tw-text-gray-900">{{__('My Profile')}}</h1>
        <x-alert/>
    </div>
    
    <div class="col-12">
        <div class="row">
            <div class="col-md-auto mb-4 mb-md-0 tw-text-center">
                <img src="{{asset($user->thumb_image)}}" data-id="profile-image" height="170" width="170" class="rounded-circle">
                <div class="my-4 tw-relative">
                    <button class="btn btn-outline-secondary tw-px-6 tw-py-2">{{__('Change Profile')}}</button>
                    <input type="file" class="tw-absolute tw-inset-0 tw-opacity-0" name="photo" form="profileForm" accept="image/*" id="file-input"/>
                </div>

                <button type="button" class="btn tw-bg-teal-100 tw-px-4 tw-py-2 text-white" data-toggle="modal" data-target="#changePasswordModal">
                    {{__('Change Password')}}
                </button>
            </div>

            <div class="col offset-md-1 tw-bg-white tw-shadow tw-rounded-md p-4 mb-5">
                <form action="{{route('users.update', ['user' => $user->hash_id])}}" class="tw-max-w-5xl" id="profileForm" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <?php 
                        $profile = $user->profile;
                        $qualification = $user->qualification;
                    ?>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="fullName">Student Full Name *</label>
                            <input type="text" name="name" class="form-control" id="fullName" value="{{ old('name', $user->name) }}">@error('name')
                                <label for="fullName" id="fullName-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="email">Student Email Address *</label>
                            <input type="email" name="email" class="form-control" id="email" value="{{ old('email', $user->email) }}">
                            @error('email')
                                <label for="email" id="email-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="guardianFullName">Parent/Guardian Full Name *</label>
                            <input type="text" name="user_profile[parent_name]" class="form-control" id="guardianFullName"  value="{{ old('user_profile.parent_name', ($profile)?$profile->parent_name:null) }}">
                            @error('user_profile.parent_name')
                                <label for="guardianFullName" id="guardianFullName-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="guardianEmail">Parent/Guardian Email Address *</label>
                            <input type="email" name="user_profile[parent_email]" class="form-control" id="guardianEmail"value="{{ old('user_profile.parent_email', ($profile)?$profile->parent_email:null) }}">@error('user_profile.parent_email')
                                <label for="guardianEmail" id="guardianEmail-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="phone">Phone Number *</label>
                            <input type="tel" name="phone" class="form-control" id="phone"   value="{{ old('user_profile.phone_no', ($profile)?$profile->phone_no:null) }}">@error('user_profile.phone_no')
                                <label for="phone" id="phone-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="streetAddress">Street Address *</label>
                            <input type="text" name="user_profile[address]" class="form-control" id="streetAddress"  value="{{ old('user_profile.address', ($profile)?$profile->address:null) }}">@error('user_profile.address')
                                <label for="streetAddress" id="streetAddress-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="city">City *</label>
                            <input type="text" name="user_profile[city]" class="form-control" id="city"  value="{{ old('user_profile.city', ($profile)?$profile->city:null) }}">@error('user_profile.city')
                                <label for="city" id="city-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="zip">State / Zip *</label>
                            <input type="text" name="user_profile[state_zip]" class="form-control" id="zip"  value="{{ old('user_profile.state_zip', ($profile)?$profile->state_zip:null) }}">@error('user_profile.state_zip')
                                <label for="zip" id="zip-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="hightSchool">High School Currently Attending *</label>
                            <input type="text" name="education[high_school_attending]" class="form-control" id="hightSchool" value="{{ old('education.high_school_attending', ($qualification)?$qualification->high_school_attending:null) }}">
                            @error('education.high_school_attending')
                                <label for="hightSchool" id="hightSchool-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="otherHightSchool">Other High School(s) Attended *</label>
                            <input type="text" name="education[other_high_school_attending]" class="form-control" id="otherHightSchool"  value="{{ old('education.other_high_school_attending', ($qualification)?$qualification->other_high_school_attending:null) }}">
                            @error('education.other_high_school_attending')
                                <label for="otherHightSchool" id="otherHightSchool-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="yearGuardian">Year of Guardian *</label>
                            <input type="text" name="education[year_of_graduation]" class="form-control" id="yearGuardian" value="{{ old('education.year_of_graduation', ($qualification)?$qualification->year_of_graduation:null) }}">
                            @error('education.year_of_graduation')
                                <label for="yearGuardian" id="yearGuardian-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="yearHightSchoolStart">Year of High School Start *</label>
                            <input type="text" name="education[high_school_start_year]" class="form-control" id="yearHightSchoolStart" value="{{ old('education.high_school_start_year', ($qualification)?$qualification->high_school_start_year:null) }}">
                            @error('education.high_school_start_year')
                                <label for="yearHightSchoolStart" id="yearHightSchoolStart-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="sport1">Sport 1 *</label>
                            <input type="text" name="education[sport1]" class="form-control" id="sport1" value="{{ old('education.sport1', ($qualification)?$qualification->sport1:null) }}">
                            @error('education.sport1')
                                <label for="sport1" id="sport1-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="sport2">Sport 2 *</label>
                            <input type="text" name="education[sport2]" class="form-control" id="sport2" value="{{ old('education.sport2', ($qualification)?$qualification->sport2:null) }}">
                            @error('education.sport2')
                                <label for="sport2" id="sport2-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="sport1Club">Sport 1 Club Team *</label>
                            <input type="text" name="education[sport_club_team1]" class="form-control" id="sport1Club" value="{{ old('education.sport_club_team1', ($qualification)?$qualification->sport_club_team1:null) }}">
                            @error('education.sport_club_team1')
                                <label for="sport1Club" id="sport1Club-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label class="tw-font-semibold" for="sport2Club">Sport 2 Club Team *</label>
                            <input type="text" name="education[sport_club_team2]" class="form-control" id="sport2Club" value="{{ old('education.sport_club_team2', ($qualification)?$qualification->sport_club_team2:null) }}">
                            @error('education.sport_club_team2')
                                <label for="sport2Club" id="sport2Club-error" class="error" role="alert">
                                    <strong>{{ $message }}</strong>
                                </label>
                            @enderror
                        </div>
                    </div>
                    <div class="row tw-mt-10">
                        <div class="form-group col-md-12">
                            <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">{{ __('Update')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>    
@endsection
<link rel="stylesheet" href="{{ asset('css/common/jquery.datetimepicker.min.css') }}"></link>
<link rel="stylesheet" href="{{ asset('build/css/intlTelInput.css') }}"></link>
@push('scripts')
    <script src="{{ asset('js/common/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/common/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('build/js/intlTelInput.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/user/update.js') }}"></script>
@endpush