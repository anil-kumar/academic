@extends('layouts.login')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-12 col-lg-8 tw-mt-16">
                <div class="tw-bg-white tw-rounded-lg tw-border-solid tw-border-2 tw-border-teal-100 tw-py-8 tw-px-5">
                    <h2 class="text-center tw-mt-0 tw-mb-6">Register Now</h2>
                    <form class="tw-max-w-2xl mx-auto">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="fullName">Student Full Name *</label>
                                <input type="text" class="form-control" id="fullName">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="email">Student Email Address *</label>
                                <input type="email" class="form-control" id="email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="guardianFullName">Parent/Guardian Full Name *</label>
                                <input type="text" class="form-control" id="guardianFullName">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="guardianEmail">Parent/Guardian Email Address *</label>
                                <input type="email" class="form-control" id="guardianEmail">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="phone">Phone Number *</label>
                                <input type="tel" class="form-control" id="phone">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="streetAddress">Street Address *</label>
                                <input type="text" class="form-control" id="streetAddress">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="city">City *</label>
                                <input type="text" class="form-control" id="city">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="zip">State / Zip *</label>
                                <input type="text" class="form-control" id="zip">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="password">Password *</label>
                                <input type="password" class="form-control" id="password">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="confirmPassword">Confirm Password *</label>
                                <input type="password" class="form-control" id="confirmPassword">
                            </div>
                        </div>
                        <div class="row tw-mt-6 tw-justify-center">
                            <div class="form-group col-md-7 text-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="disabledFieldsetCheck" disabled>
                                    <label class="form-check-label" for="disabledFieldsetCheck">
                                        {{__('By Clicking "Sign Up" button, you agree with our')}}
                                        <a href="" class="tw-underline text-muted">{{__('Terms and conditions')}}</a> {{__('and')}}
                                        <a href="" class="tw-underline text-muted">{{__('Privacy Policy')}}</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row tw-mt-10">
                            <div class="form-group col-md-12 text-center">
                                <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">{{ __('Register Now')}}</button>
                            </div>
                        </div>
                        <p class="text-center tw-mt-2 tw-font-semibold">
                            {{__("Already have an account?")}}
                            <a href="sign-In" class="tw-text-teal-100 hover:tw-text-teal-100">{{__('Sign In')}}</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
