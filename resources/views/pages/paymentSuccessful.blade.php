@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('Packages')}}</h1>
        </div>
        
        <div class="mb-5">
            <div class="row">
                
                <div class="col-md-6 mb-5 mx-auto">
                    <div class="card">
                        <div class="card-body text-center my-4">
                            <svg width="98" height="98" viewBox="0 0 98 98" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M49 98C76.062 98 98 76.062 98 49C98 21.938 76.062 0 49 0C21.938 0 0 21.938 0 49C0 76.062 21.938 98 49 98ZM75.1213 34.1213C76.2929 32.9497 76.2929 31.0503 75.1213 29.8787C73.9497 28.7071 72.0503 28.7071 70.8787 29.8787L40 60.7574L27.1213 47.8787C25.9497 46.7071 24.0503 46.7071 22.8787 47.8787C21.7071 49.0503 21.7071 50.9497 22.8787 52.1213L37.8787 67.1213C39.0503 68.2929 40.9497 68.2929 42.1213 67.1213L75.1213 34.1213Z" fill="#38B2AC"/>
                            </svg>
                            <h3 class="my-4">{{__('Payment Successful')}}</h3>
                            <p>{{__('Dear John, Your package has been successfully added. An email will be sent shortly with a receipt and booking details. Thank you!')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mb-5 mx-auto">
                    <div class="card">
                        <div class="card-body text-center my-4">
                            <div class="tw-flex">
                                <span class="mr-3">{{__('Package Name:')}}</span>
                                <span class="mr-3 tw-font-semibold">{{__('Whole Semester')}}</span>
                            </div>
                            <div class="tw-flex">
                                <span class="mr-3">{{__('Package Cost:')}}</span>
                                <span class="mr-3 tw-font-semibold">{{__('$ 180')}}</span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    
@endsection