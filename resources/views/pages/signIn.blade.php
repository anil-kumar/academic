@extends('layouts.login')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6 tw-mt-16">
                <div class="tw-bg-white tw-rounded-lg tw-border-solid tw-border-2 tw-border-teal-100 tw-py-8 tw-px-5">
                    <h2 class="text-center tw-mt-0 tw-mb-6">Login</h2>
                    <form class="tw-max-w-sm mx-auto">
                        <div class="form-group">
                            <label class="tw-font-semibold" for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label class="tw-font-semibold" for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1">
                        </div>
                        <div class="tw-flex tw-justify-between tw-items-center tw-mt-5">
                            <div>
                                <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">{{ __('Sign In')}}</button>
                            </div>
                            <a href="" class="tw-text-teal-100 tw-font-semibold hover:tw-text-teal-100">{{__('Forgot Password?')}}</a>
                        </div>
                        <p class="text-center tw-mt-8 tw-font-semibold">
                            {{__("Don't have an account?")}}
                            <a href="signup" class="tw-text-teal-100 hover:tw-text-teal-100">{{__('Sign Up')}}</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
