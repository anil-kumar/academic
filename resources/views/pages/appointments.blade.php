@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('Appointments')}}</h1>
            <button type="button" class="btn tw-bg-teal-100 tw-px-4 tw-py-2 text-white" data-toggle="modal" data-target="#bookAppointmentModal">
                {{__('Book Appointment')}}
            </button>
        </div>
        
        <section class="mb-5">
            <div class="table-responsive-xl bg-white tw-rounded-md tw-shadow">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col" class="tw-uppercase small">{{__('Package Name')}}</th>
                            <th scope="col" class="tw-uppercase small">{{__('Date of Appointment')}}</th>
                            <th scope="col" class="tw-uppercase small text-right">{{__('Time')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{__('Number theory')}}</td>
                            <td>{{__('April 7, 2020')}}</td>
                            <td class="text-right">
                                {{ __('2:30 PM')}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>

    </div>
    
@endsection