@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('Recorded Sessions')}}</h1>
        </div>
        
        <section class="mb-5">
            <div class="table-responsive-xl bg-white tw-rounded-md tw-shadow">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col" class="tw-uppercase small">{{__('Package Name')}}</th>
                            <th scope="col" class="tw-uppercase small">{{__('Recorded Date')}}</th>
                            <th scope="col" class="tw-uppercase small">{{__('File Name')}}</th>
                            <th scope="col" class="tw-uppercase small text-right">{{__('Actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{__('Whole semester')}}</td>
                            <td>{{__('April 07, 2020')}}</td>
                            <td>{{__('xyz.text')}}</td>
                            <td class="text-right tw-w-64">
                                <span class="d-inline-block">
                                    <a href="" class="tw-text-teal-100 hover:tw-text-teal-100 tw-flex tw-items-center tw-block">
                                        <svg class="tw-mr-2 tw-h-4 tw-w-4" fill="currentColor" viewBox="0 0 20 20">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                                <g id="icon-shape">
                                                    <path d="M13,8 L13,2 L7,2 L7,8 L2,8 L10,16 L18,8 L13,8 Z M0,18 L20,18 L20,20 L0,20 L0,18 Z" id="Combined-Shape"></path>
                                                </g>
                                            </g>
                                        </svg>
                                        {{ __('Download') }}
                                    </a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>{{__('Number theory')}}</td>
                            <td>{{__('April 07, 2020')}}</td>
                            <td>{{__('xyz.text')}}</td>
                            <td class="text-right tw-w-64">
                                <span class="d-inline-block">
                                    <a href="" class="tw-text-teal-100 hover:tw-text-teal-100 tw-flex tw-items-center tw-block">
                                        <svg class="tw-mr-2 tw-h-4 tw-w-4" fill="currentColor" viewBox="0 0 20 20">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                                <g id="icon-shape">
                                                    <path d="M13,8 L13,2 L7,2 L7,8 L2,8 L10,16 L18,8 L13,8 Z M0,18 L20,18 L20,20 L0,20 L0,18 Z" id="Combined-Shape"></path>
                                                </g>
                                            </g>
                                        </svg>
                                        {{ __('Download') }}
                                    </a>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>

    </div>
    
@endsection