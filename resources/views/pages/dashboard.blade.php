@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('Dashboard')}}</h1>
        </div>

        <section class="mb-5">
            <h3 class="tw-text-xl tw-mb-4">{{__('Tasks')}}</h3>
            <div class="table-responsive-xl bg-white tw-rounded-md tw-shadow">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col" class="tw-uppercase small">{{__('Task Name')}}</th>
                            <th scope="col" class="tw-uppercase small">{{__('Created By')}}</th>
                            <th scope="col" colspan="2" class="tw-uppercase small">{{__('Status')}}</th>
                            <th scope="col" class="tw-uppercase small text-right">{{__('Actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Calculus and analysis</td>
                            <td>Student</td>
                            <td class="tw-text-teal-100">Completed</td>
                            <td class="tw-w-64"></td>
                            <td class="text-right tw-w-64">
                                <span class="d-inline-block">
                                    <a href="" class="tw-text-red-500 hover:tw-text-red-500 tw-flex tw-items-center tw-block">
                                        <svg class="tw-mr-2 tw-h-4 tw-w-4" fill="currentColor" viewBox="0 0 20 20">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                                <g id="icon-shape">
                                                    <path d="M2,2 L18,2 L18,4 L2,4 L2,2 Z M8,0 L12,0 L14,2 L6,2 L8,0 Z M3,6 L17,6 L16,20 L4,20 L3,6 Z M8,8 L9,8 L9,18 L8,18 L8,8 Z M11,8 L12,8 L12,18 L11,18 L11,8 Z" id="Combined-Shape"></path>

                                                </g>
                                            </g>
                                        </svg>
                                        {{ __('Delete') }}
                                    </a>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>Calculus and analysis</td>
                            <td>Admin</td>
                            <td class="tw-text-yellow-600">Pending</td>
                            <td class="tw-w-64">
                                <div class="form-check mr-5">
                                    <input class="form-check-input tw-mt-2" type="checkbox" id="disabledFieldsetCheck" disabled>
                                    <label class="form-check-label" for="disabledFieldsetCheck">
                                        {{__('Mark as completed')}}
                                    </label>
                                </div>
                            </td>
                            <td class="text-right tw-w-64">
                                <span class="d-inline-block">
                                    <a href="" class="tw-text-gray-900 hover:tw-text-gray-900 tw-flex tw-items-center tw-block">
                                        <svg class="tw-mr-2 tw-h-4 tw-w-4" fill="currentColor" viewBox="0 0 20 20">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                                <g id="icon-shape">
                                                    <path d="M19.8005808,10 C17.9798698,6.43832409 14.2746855,4 10,4 C5.72531453,4 2.02013017,6.43832409 0.199419187,10 C2.02013017,13.5616759 5.72531453,16 10,16 C14.2746855,16 17.9798698,13.5616759 19.8005808,10 Z M10,14 C12.209139,14 14,12.209139 14,10 C14,7.790861 12.209139,6 10,6 C7.790861,6 6,7.790861 6,10 C6,12.209139 7.790861,14 10,14 Z M10,12 C11.1045695,12 12,11.1045695 12,10 C12,8.8954305 11.1045695,8 10,8 C8.8954305,8 8,8.8954305 8,10 C8,11.1045695 8.8954305,12 10,12 Z" id="Combined-Shape"></path>
                                                </g>
                                            </g>
                                        </svg>
                                        {{ __('View') }}
                                    </a>
                                </span>
                                <span class="d-inline-block tw-ml-4">
                                    <a href="" class="tw-text-red-500 hover:tw-text-red-500 tw-flex tw-items-center tw-block">
                                        <svg class="tw-mr-2 tw-h-4 tw-w-4" fill="currentColor" viewBox="0 0 20 20">
                                            <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                                <g id="icon-shape">
                                                    <path d="M2,2 L18,2 L18,4 L2,4 L2,2 Z M8,0 L12,0 L14,2 L6,2 L8,0 Z M3,6 L17,6 L16,20 L4,20 L3,6 Z M8,8 L9,8 L9,18 L8,18 L8,8 Z M11,8 L12,8 L12,18 L11,18 L11,8 Z" id="Combined-Shape"></path>

                                                </g>
                                            </g>
                                        </svg>
                                        {{ __('Delete') }}
                                    </a>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        
        <section class="mb-5">
            <div class="tw-flex tw-justify-between tw-items-center tw-mb-4">
                <h3 class="tw-text-xl mb-0">{{__('Upcoming Appointments')}}</h3>
                <button type="button" class="btn tw-bg-teal-100 tw-px-4 tw-py-2 text-white" data-toggle="modal" data-target="#bookAppointmentModal">
                    {{__('Book Appointment')}}
                </button>
            </div>
            <div class="table-responsive-xl bg-white tw-rounded-md tw-shadow">
                <table class="table mb-0">
                    <thead>
                        <tr>
                            <th scope="col" class="tw-uppercase small">{{__('Package Name')}}</th>
                            <th scope="col" class="tw-uppercase small">{{__('Date of Appointment')}}</th>
                            <th scope="col" class="tw-uppercase small text-right">{{__('Time')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{__('Number theory')}}</td>
                            <td>{{__('April 7, 2020')}}</td>
                            <td class="text-right">
                                {{ __('2:30 PM')}}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        
        <section class="mb-5">
            <h3 class="tw-text-xl tw-mb-4">{{__('Quote/Video of the Day')}}</h3>
            <video controls poster="https://blog.cudoo.com/wp-content/uploads/2018/03/social-language-learning-1.png">
                <source src="movie.mp4" type="video/mp4">
                <source src="movie.ogg" type="video/ogg">
                Your browser does not support the video tag.
            </video>
        </section>

    </div>
    
@endsection