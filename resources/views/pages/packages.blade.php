@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('Packages')}}</h1>
        </div>
        
        <div class="mb-5">
            <div class="row">
                
                <div class="col-md-6 mb-5 mb-md-0">
                    <div class="card">
                        <div class="card-header tw-bg-white tw-flex tw-justify-between">
                            <h5 class="card-title mb-0 tw-uppercase tw-font-semibold">{{ __('Whole semester')}}</h5>
                            <span class="h5 mb-0">{{__('$ 180')}}</span>
                        </div>
                        <div class="card-body text-center">
                            <ul class="tw-list-none text-left">
                                <li>
                                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-teal-100 tw-h-5 tw-mr-3">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                            <g id="icon-shape">
                                                <polygon id="Path-126" points="0 11 2 9 7 14 18 3 20 5 7 18"></polygon>							
                                            </g>
                                        </g>
                                    </svg>
                                    {{__('Lorem ipsum dolor sit amet, consectetuer adipiscing elit.')}}
                                </li>
                                <li>
                                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-teal-100 tw-h-5 tw-mr-3">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                            <g id="icon-shape">
                                                <polygon id="Path-126" points="0 11 2 9 7 14 18 3 20 5 7 18"></polygon>							
                                            </g>
                                        </g>
                                    </svg>
                                    {{__('Aliquam tincidunt mauris eu risus.')}}
                                </li>
                                <li>
                                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-teal-100 tw-h-5 tw-mr-3">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                            <g id="icon-shape">
                                                <polygon id="Path-126" points="0 11 2 9 7 14 18 3 20 5 7 18"></polygon>							
                                            </g>
                                        </g>
                                    </svg>
                                    {{__('Vestibulum auctor dapibus neque.')}}
                                </li>
                            </ul>

                            <a href="paymentSuccessful" class="btn mt-4 tw-bg-teal-100 hover:tw-bg-teal-100 tw-text-white hover:tw-text-white tw-px-4 tw-py-2" type="button">{{__('Choose Package')}}</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-5 mb-md-0">
                    <div class="card">
                        <div class="card-header tw-bg-white tw-flex tw-justify-between">
                            <h5 class="card-title mb-0 tw-uppercase tw-font-semibold">{{ __('Virtual learning')}}</h5>
                            <span class="h5 mb-0">{{__('$ 130')}}</span>
                        </div>
                        <div class="card-body text-center">
                            <ul class="tw-list-none text-left">
                                <li>
                                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-teal-100 tw-h-5 tw-mr-3">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                            <g id="icon-shape">
                                                <polygon id="Path-126" points="0 11 2 9 7 14 18 3 20 5 7 18"></polygon>							
                                            </g>
                                        </g>
                                    </svg>
                                    {{__('Lorem ipsum dolor sit amet, consectetuer adipiscing elit.')}}
                                </li>
                                <li>
                                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-teal-100 tw-h-5 tw-mr-3">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                            <g id="icon-shape">
                                                <polygon id="Path-126" points="0 11 2 9 7 14 18 3 20 5 7 18"></polygon>							
                                            </g>
                                        </g>
                                    </svg>
                                    {{__('Aliquam tincidunt mauris eu risus.')}}
                                </li>
                                <li>
                                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-teal-100 tw-h-5 tw-mr-3">
                                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                                            <g id="icon-shape">
                                                <polygon id="Path-126" points="0 11 2 9 7 14 18 3 20 5 7 18"></polygon>							
                                            </g>
                                        </g>
                                    </svg>
                                    {{__('Vestibulum auctor dapibus neque.')}}
                                </li>
                            </ul>

                            <a href="paymentSuccessful" class="btn mt-4 tw-bg-teal-100 hover:tw-bg-teal-100 tw-text-white hover:tw-text-white tw-px-4 tw-py-2" type="button">{{__('Choose Package')}}</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
    
@endsection