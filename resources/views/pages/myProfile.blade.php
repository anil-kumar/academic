@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('My Profile')}}</h1>
            <x-alert/>          
        </div>
        
        <div class="col-12">
            <div class="row">
                <div class="col-md-auto mb-4 mb-md-0 tw-text-center">
                    <svg viewBox="0 0 20 20" class="tw-fill-current tw-text-gray-800 tw-h-56">
                        <g id="Page-1" stroke="none" stroke-width="1" fill-rule="evenodd">
                            <g id="icon-shape">
                                <path d="M10,20 C15.5228475,20 20,15.5228475 20,10 C20,4.4771525 15.5228475,0 10,0 C4.4771525,0 0,4.4771525 0,10 C0,15.5228475 4.4771525,20 10,20 Z M6.99999861,6.00166547 C6.99999861,4.34389141 8.3465151,3 9.99999722,3 C11.6568507,3 12.9999958,4.33902013 12.9999958,6.00166547 L12.9999958,7.99833453 C12.9999958,9.65610859 11.6534793,11 9.99999722,11 C8.34314374,11 6.99999861,9.66097987 6.99999861,7.99833453 L6.99999861,6.00166547 Z M3.34715433,14.4444439 C5.37306718,13.5169611 7.62616222,13 10,13 C12.3738388,13 14.6269347,13.5169615 16.6528458,14.4444437 C15.2177146,16.5884188 12.7737035,18 10,18 C7.22629656,18 4.78228556,16.5884189 3.34715433,14.4444439 L3.34715433,14.4444439 Z" id="Combined-Shape"></path>				
                            </g>
                        </g>
                    </svg>

                    <div class="my-4 tw-relative">
                        <button class="btn btn-outline-secondary tw-px-6 tw-py-2">{{__('Change Profile')}}</button>
                        <input type="file" class="tw-absolute tw-inset-0 tw-opacity-0" name="myfile" />
                    </div>

                    <button type="button" class="btn tw-bg-teal-100 tw-px-4 tw-py-2 text-white" data-toggle="modal" data-target="#changePasswordModal">
                        {{__('Change Password')}}
                    </button>
                </div>

                <div class="col offset-md-1 tw-bg-white tw-shadow tw-rounded-md p-4 mb-5">
                    <form class="tw-max-w-5xl">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="fullName">Student Full Name *</label>
                                <input type="text" class="form-control" id="fullName">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="email">Student Email Address *</label>
                                <input type="email" class="form-control" id="email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="guardianFullName">Parent/Guardian Full Name *</label>
                                <input type="text" class="form-control" id="guardianFullName">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="guardianEmail">Parent/Guardian Email Address *</label>
                                <input type="email" class="form-control" id="guardianEmail">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="phone">Phone Number *</label>
                                <input type="tel" class="form-control" id="phone">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="streetAddress">Street Address *</label>
                                <input type="text" class="form-control" id="streetAddress">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="city">City *</label>
                                <input type="text" class="form-control" id="city">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="zip">State / Zip *</label>
                                <input type="text" class="form-control" id="zip">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="hightSchool">High School Currently Attending *</label>
                                <input type="text" class="form-control" id="hightSchool">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="otherHightSchool">Other High School(s) Attended *</label>
                                <input type="text" class="form-control" id="otherHightSchool">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="yearGuardian">Year of Guardian *</label>
                                <input type="text" class="form-control" id="yearGuardian">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="yearHightSchoolStart">Year of High School Start *</label>
                                <input type="text" class="form-control" id="yearHightSchoolStart">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="sport1">Sporot 1 *</label>
                                <input type="text" class="form-control" id="sport1">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="sport2">Sport 2 *</label>
                                <input type="text" class="form-control" id="sport2">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="sport1Club">Sporot 1 Club Team *</label>
                                <input type="text" class="form-control" id="sport1Club">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="tw-font-semibold" for="sport2Club">Sport 2 Club Team *</label>
                                <input type="text" class="form-control" id="sport2Club">
                            </div>
                        </div>
                        <div class="row tw-mt-10">
                            <div class="form-group col-md-12">
                                <button type="submit" class="tw-px-6 tw-py-2 tw-text-white tw-bg-teal-100 btn hover:tw-text-teal-200">{{ __('Update')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    
@endsection