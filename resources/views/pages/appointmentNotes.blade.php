@extends('layouts.client')
@section('content')
    <div class="container-fluid tw-px-6">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 tw-text-gray-900">{{__('Appointment Notes')}}</h1>
        </div>
        
        <section class="mb-5">
            <div class="card">
                <div class="card-header tw-bg-white">
                    <h4 class="card-title mb-0">{{ __('Session Name')}}
                        <small class="tw-text-gray-500 tw-font-semibold">{{__('( Jan 08, 2020 )')}}</small>
                    </h4>
                </div>
                <div class="card-body">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia distinctio illum fugit voluptatem! Tempora voluptas mollitia et aut dolorem repellat excepturi amet fugiat minima numquam ea facilis cum, eius officiis maxime sequi commodi saepe architecto! Corrupti id aspernatur reiciendis perspiciatis libero nihil rerum neque non reprehenderit modi doloribus accusantium, provident quae distinctio fuga, blanditiis totam sint itaque temporibus! Minima in nihil quod modi ducimus ut esse natus illo laboriosam cum ex eum corporis quos repellat adipisci, alias similique quibusdam! Corporis sint rem ratione magnam odit dolorem sit, delectus sequi exercitationem voluptatum veniam, saepe maxime voluptatibus qui magni ab. Illo, sit.</p>
                </div>
            </div>
        </section>

        <section class="mb-5">
            <div class="card">
                <div class="card-header tw-bg-white">
                    <h4 class="card-title mb-0">{{ __('Session Name')}}
                        <small class="tw-text-gray-500 tw-font-semibold">{{__('( Jan 08, 2020 )')}}</small>
                    </h4>
                </div>
                <div class="card-body">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia distinctio illum fugit voluptatem! Tempora voluptas mollitia et aut dolorem repellat excepturi amet fugiat minima numquam ea facilis cum, eius officiis maxime sequi commodi saepe architecto! Corrupti id aspernatur reiciendis perspiciatis libero nihil rerum neque non reprehenderit modi doloribus accusantium, provident quae distinctio fuga, blanditiis totam sint itaque temporibus! Minima in nihil quod modi ducimus ut esse natus illo laboriosam cum ex eum corporis quos repellat adipisci, alias similique quibusdam! Corporis sint rem ratione magnam odit dolorem sit, delectus sequi exercitationem voluptatum veniam, saepe maxime voluptatibus qui magni ab. Illo, sit.</p>
                </div>
            </div>
        </section>

    </div>
    
@endsection