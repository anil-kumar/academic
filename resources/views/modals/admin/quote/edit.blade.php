<!-- Modal -->
<div class="modal fade" id="editQuoteModal" tabindex="-1" role="dialog" aria-labelledby="editQuoteLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Quote/Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('admin.quote-videos.store') }}" id="EditQuoteForm" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="EditQuote">{{ __('Edit Quote') }}</label>
                        <textarea name="quote" class="form-control" id="EditQuote" required></textarea>
                        @error('quote')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="EditTitle">{{ __('Video title') }}</label>
                        <input name="title" class="form-control" id="EditTitle" required>
                        @error('title')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="EditVideo">{{ __('Add Video') }}</label>
                        <input type="file" name="video" class="form-control" id="EditVideo" accept='video/*, .mkv, .avi, .flv, .3gp'>
                        @error('video')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="EditDisplay">{{ __('Set date/time to display on client dashboard') }}</label>
                        <input name="display_at" class="form-control" id="EditDisplay" required>
                        @error('video')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="EditQuoteForm" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>