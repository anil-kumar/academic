<!-- Modal -->
<div class="modal fade" id="addQuoteModal" tabindex="-1" role="dialog" aria-labelledby="addQuoteLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Quote/Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('admin.quote-videos.store') }}" id="QuoteForm" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="Quote">{{ __('Add Quote') }}</label>
                        <textarea name="quote" class="form-control" id="Quote" required></textarea>
                        @error('quote')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="Description">{{ __('Video title') }}</label>
                        <input name="title" class="form-control" id="Title" required>
                        @error('title')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="Video">{{ __('Add Video') }}</label>
                        <input type="file" name="video" class="form-control" id="Video" required  accept='video/*, .mkv, .avi, .flv, .3gp'>
                        @error('video')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="Display">{{ __('Set date/time to display on client dashboard') }}</label>
                        <input name="display_at" class="form-control" id="display" required>
                        @error('video')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="QuoteForm" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>