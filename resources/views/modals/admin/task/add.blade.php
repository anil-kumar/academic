<!-- Modal -->
<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="addTaskModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('admin.tasks.store') }}" id="TaskForm">
                    @csrf
                    <div class="form-group">
                        <label for="Name">{{ __('Task Name') }}</label>
                        <input name="name" class="form-control" id="Name" required>
                        @error('name')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="Description">{{ __('Description') }}</label>
                        <textarea name="description" class="form-control" id="Description" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="TaskForm" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>