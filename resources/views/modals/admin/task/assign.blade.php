<!-- Modal -->
<div class="modal fade" id="assignTaskModal" tabindex="-1" role="dialog" aria-labelledby="editTaskModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{route('admin.user-tasks.store')}}" id="AssignTaskForm">
                    @csrf
                    <div class="form-group">
                        <label for="UserID">{{ __('Select Client') }}</label>
                        <select name="user_id" class="form-control" id="UserID" required>
                            @foreach($users as $user)
                                <option value="{{$user->id}}">
                                    {{$user->full_name}}
                                </option>
                            @endforeach
                        </select>
                        @error('name')
                            <label class="">{{ $message }}</label>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="TaskId">{{ __('Select Task') }}</label>
                        <select name="task_id" class="form-control" id="TaskId" required>
                            @foreach($tasks as $task)
                                <option value="{{$task->id}}">
                                    {{$task->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="TaskId">{{ __('Task Date') }}</label>
                        <input name="task_date" class="form-control" id="TaskDate" required>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" form="AssignTaskForm" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>