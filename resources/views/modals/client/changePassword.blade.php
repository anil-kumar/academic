<!-- Modal -->
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Change Password')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('change-password') }}" enctype="multipart/form-data" id='edit-password' method="post">
                    @csrf
                    <div class="form-group">
                        <label for="oldPassword">{{ __('Current Password') }}</label>
                        <input type="password" name="password" class="form-control"
                            id="oldPassword">
                        @error('password')
                            <span class="text-danger" data-error='error' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="new-password">New Password</label>
                        <input type="password" name="new_password" class="form-control"
                            id='new-password'>
                        @error('new_password')
                            <span class="text-danger" data-error='error' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="confirm-password">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="confirm-password">
                        @error('password_confirmation')
                            <span class="text-danger" data-error='error' role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn tw-bg-gray-600 tw-text-white hover:tw-text-white tw-px-4 tw-py-2" data-dismiss="modal">Close</button>
                <button type="submit" form="edit-password" class="btn tw-bg-teal-100 tw-text-white hover:tw-text-white tw-px-4 tw-py-2">Update</button>
            </div>
        </div>
    </div>
</div>