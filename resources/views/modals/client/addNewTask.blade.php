<!-- Modal -->
<div class="modal fade" id="addNewTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Add New Task')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="taskName">{{__('Task Name')}}</label>
                        <input type="text" class="form-control" id="taskName">
                    </div>
                    <div class="form-group">
                        <label for="taskDate">{{__('Task Date')}}</label>
                        <input type="date" class="form-control" id="taskDate">
                    </div>
                    <div class="form-group">
                        <label for="taskDescription">{{__('Task Description')}}</label>
                        <textarea rows="5" class="form-control" id="taskDescription"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn tw-bg-gray-600 tw-text-white hover:tw-text-white tw-px-4 tw-py-2" data-dismiss="modal">Close</button>
                <button type="button" class="btn tw-bg-teal-100 tw-text-white hover:tw-text-white tw-px-4 tw-py-2">Save</button>
            </div>
        </div>
    </div>
</div>