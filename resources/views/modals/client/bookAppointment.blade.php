<!-- Modal -->
<div class="modal fade" id="bookAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{__('Book Appointment')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning tw-flex tw-justify-between tw-items-center" role="alert">
                    <span>{{__('Free Session Left')}}</span>
                    <span>{{__('2')}}</span>
                </div>

                <form>
                    <div class="form-group">
                        <label for="studentName">{{__('Student Name')}}</label>
                        <input type="text" class="form-control" id="studentName">
                    </div>
                    <div class="form-group">
                        <label for="appointmentDate">{{__('Date of Appointment')}}</label>
                        <input type="date" class="form-control" id="appointmentDate">
                    </div>
                    <div class="form-group">
                        <label for="appointmentTime">{{__('Time')}}</label>
                        <input type="time" class="form-control" id="appointmentTime">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn tw-bg-gray-600 tw-text-white hover:tw-text-white tw-px-4 tw-py-2" data-dismiss="modal">{{__('Close')}}</button>
                <button type="button" class="btn tw-bg-teal-100 tw-text-white hover:tw-text-white tw-px-4 tw-py-2">{{__('Confirm Appointment')}}</button>
            </div>
        </div>
    </div>
</div>