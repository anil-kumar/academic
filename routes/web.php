<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/sign-In', function () {
	return view('pages.signIn');
});

Route::get('/signup', function () {
	return view('pages.signup');
});

Route::get('/dashboard', function () {
	return view('pages.dashboard');
})->name('dashboard')->middleware('auth')->middleware('verified');

Route::get('/tasks', function () {
	return view('pages.tasks');
});

Route::get('/appointments', function () {
	return view('pages.appointments');
});

Route::get('/appointmentNotes', function () {
	return view('pages.appointmentNotes');
});

Route::get('/myProfile', function () {
	return view('pages.myProfile');
});

Route::get('/uploads', function () {
	return view('pages.uploads');
});

Route::get('/packages', function () {
	return view('pages.packages');
});

Route::get('/paymentSuccessful', function () {
	return view('pages.paymentSuccessful');
});

Route::get('/sessionVideo', function () {
	return view('pages.recordedSessionVideo');
});


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::post('change-password', 'ChangePasswordController@save')->name('change-password');

Route::resource('/users', 'UserController');
// Route::resource('/tasks', 'TaskController');
// Route::resource('/packages', 'PackageController');
// Route::resource('/appointments', 'AppointmentsController');

// Controllers Within The "App\Http\Controllers\Admin" Namespace
Route::namespace('Admin')
	->prefix('admin')
	->name('admin.')
	->group(function () {
		Route::get('/', function () {
	        return view('admin.login');
	    })
	    ->name('login')
	    ->middleware('guest');

		Route::get('/dashboard', function () {
	        return view('admin.dashboard');
	    })->name('dashboard');
	    
	    Route::get('change-password', 'ChangePasswordController@index')
			->name('password.change');
		Route::post('change-password', 'ChangePasswordController@store')
			->name('password.update');
	    
		//Resource routes
		Route::resource('/users', 'UserController');
		Route::resource('/packages', 'PackageController');
		Route::resource('/tasks', 'TaskController');
		Route::resource('/quote-videos', 'QuoteVideoController');
		Route::resource('/user-profiles', 'UserProfileController');
		Route::resource('/user-tasks', 'UserTaskController');
		Route::resource('/appointments', 'AppointmentController');
		Route::resource('/user-forms', 'UsersFromController');
	});
