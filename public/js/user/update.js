(function(){

	$('#file-input').on('change', function(){
		updateImageDisplay(this,'profile-image')
	});

	function updateImageDisplay(input, imagepreview) {
	    var curFiles = input.files;
	    if (curFiles.length === 0) {
	        console.log('No files currently selected for upload')
	    } else {
	    	var image = document.querySelector(`img[data-id="${imagepreview}"]`);
	        for (var i = 0; i < curFiles.length; i++) {
	            image.src = window.URL.createObjectURL(curFiles[i]);
	        }
	    }
	}

	$('#profileForm').validate({
		rules: {
			'name':{
				required:true,
				minlength:3,
			},
			'email':{
				email:true,
				required:true,
			},
			'password':{
				required:true,
				minlength:3,
			},
			'password_confirmation':{
				equalTo:'#password',
				required:true,
			},
			'user_profile[parent_name]':{
				required:true,
				minlength:3,
			},
			'user_profile[parent_email]':{
				email:true,
				required:true,
				minlength:3,
			},
			'phone':{
				required:true,
				minlength:9,
				maxlength:15,
			},
			'user_profile[address]':{
				required:true,
				minlength:3,
			},
			'user_profile[city]':{
				required:true,
				minlength:3,
			},
			'user_profile[state_zip]':{
				required:true,
				minlength:3,
			},
			'education[high_school_attending]':{
				required:true,
				minlength:3,
			},
			'education[other_high_school_attending]':{
				required:true,
				minlength:3,
			},
			'education[year_of_graduation]':{
				required:true,
				minlength:3,
			},
			'education[high_school_start_year]':{
				required:true,
				minlength:3,
			}
		}
	})

	$('#yearGuardian').validate({
		rules: {
			'password':{
				required:true,
				minlength:8
			},
			'new_password':{
				required:true,
				minlength:8
			},
			'password_confirmation':{
				required:true,
				minlength:8,
				equalTo:'new-password'
			},
		}
	});

	$('#yearGuardian').datetimepicker({
		format:'Y-m-d'
	});
	
	$('#yearHightSchoolStart').datetimepicker({
		format:'Y-m-d'
	});

	var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      hiddenInput: "user_profile[phone_no]",
      initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "build/js/utils.js",
    });
})()