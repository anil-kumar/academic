(function(){
	$('#RegisterForm').validate({
		rules: {
			'name':{
				required:true,
				minlength:3,
			},
			'email':{
				email:true,
				required:true,
			},
			'password':{
				required:true,
				minlength:3,
			},
			'password_confirmation':{
				equalTo:'#password',
				required:true,
			},
			'user_profile[parent_name]':{
				required:true,
				minlength:3,
			},
			'user_profile[parent_email]':{
				email:true,
				required:true,
				minlength:3,
			},
			'phone':{
				required:true,
				minlength:9,
				maxlength:12,
				number:true
			},
			'user_profile[address]':{
				required:true,
				minlength:3,
			},
			'user_profile[city]':{
				required:true,
				minlength:3,
			},
			'user_profile[state_zip]':{
				required:true,
				minlength:3,
			}
		}
	});
	var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      hiddenInput: "user_profile[phone_no]",
      initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "build/js/utils.js",
    });
})()