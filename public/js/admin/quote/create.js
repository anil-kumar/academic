(function(){
	$('#display').datetimepicker({
		format:'Y-m-d H:i'
	});
	$('#EditDisplay').datetimepicker({
		format:'Y-m-d H:i'
	});
	function returnFileSize(number) {
	  	if(number < 1024) {
	    	return number + 'bytes';
	  	} else if(number >= 1024 && number < 1048576) {
	    	return (number/1024).toFixed(1) + 'KB';
	  	} else if(number >= 1048576) {
	    	return (number/1048576).toFixed(1) + 'MB';
	  	}
	}
	jQuery.validator.addMethod("filesize_max", function(value, element, param) {
	    var isOptional = this.optional(element),
	        file;
	    
	    if(isOptional) {
	        return isOptional;
	    }
	    
	    if ($(element).attr("type") === "file") {
	        
	        if (element.files && element.files.length) {
	            
	            file = element.files[0];
	            console.log(file.size, (file.size/1048576).toFixed(1),"mb");

	            return ( file.size && (file.size/1048576).toFixed(1) <= param ); 
	        }
	    }
	    return false;
	}, "File size is too large. File must be less then 250MB");

	$('#QuoteForm').validate({
		rules: {
			'quote':{
				required:true,
				minlength:3,
			},
			'title':{
				required:true,
				minlength:3
			},
			'video':{
				required:true,
				filesize_max: 250 //mb
			},
			'display_at':{
				required:true
			},
	    }
    });
	$('#EditQuoteForm').validate({
		rules: {
			'quote':{
				required:true,
				minlength:3,
			},
			'title':{
				required:true,
				minlength:3
			},
			'display_at':{
				required:true
			},
			'video':{
				filesize_max: 250 //mb
			},
	    }
    });


	$(document).on('click', '.fa-edit', function() {
    	let quadrant = $(this).attr('data-obj');
    	let url = $(this).attr('data-url');
    	let form = document.getElementById('EditQuoteForm');
    	if(form) {
	    	form.reset();
	    	form.action = url;
	    	obj = JSON.parse(atob(quadrant));
	    	form.querySelector('input[name="title"]').value = obj.title
	    	form.querySelector('input[name="display_at"]').value = obj.display_at
	    	form.querySelector('textarea[name="quote"]').value = obj.quote
    		$('#editQuoteModal').modal('show');
    	}
    });
})()