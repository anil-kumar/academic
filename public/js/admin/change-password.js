$( function() {

	$('#ChangePasswordForm').validate({
		rules: {
			'current_password':{
				required:true,
				minlength:8,
			},
			'new_password':{
				required:true,
				minlength:8
			},
			'new_confirm_password':{
				required:true,
				minlength:8,
				equalTo:"#NewPassword"
			}
	    }
    });
});