$( function() {
	jQuery.validator.addMethod("greaterThan", function(value, element, params) {
	    if (!/Invalid|NaN/.test(new Date(value))) {
	        return new Date(value) > new Date($(params).val());
	    }

	    return isNaN(value) && isNaN($(params).val()) 
	        || (Number(value) > Number($(params).val())); 
	},'Must be greater than from.');

	jQuery.validator.addMethod("greaterTime", function(value, element, params) {
		console.log(params,$(params).val());
	    return Compare($(params).val(), element.value); 
	},'Must be greater than from.');

	$('#PackageForm').validate({
		rules: {
			'name':{
				required:true,
				minlength:3,
			},
			'description':{
				required:true,
				minlength:3
			},
			'package_price':{
				required:true,
				min:1
			},
			'session_price':{
				required:true,
				min:1
			},
			'session_duration':{
				required:true
			},
			'date_from':{
				required:true
			},
			'date_to':{
				required:true,
				greaterThan:"#PackageFrom"
			},
			'availability[0][time_from]':{
				required:true
			},
			'availability[0][time_to]':{
				required:true,
				greaterTime:"input[name='availability[0][time_from]']"
			},
			'availability[1][time_from]':{
				required:true
			},
			'availability[1][time_to]':{
				required:true,
				greaterTime:"input[name='availability[1][time_from]']"
			},
			'availability[2][time_from]':{
				required:true
			},
			'availability[2][time_to]':{
				required:true,
				greaterTime:"input[name='availability[2][time_from]']"
			},
			'availability[3][time_from]':{
				required:true
			},
			'availability[3][time_to]':{
				required:true,
				greaterTime:"input[name='availability[3][time_from]']"
			},
			'availability[4][time_from]':{
				required:true
			},
			'availability[4][time_to]':{
				required:true,
				greaterTime:"input[name='availability[4][time_from]']"
			},
			'availability[5][time_from]':{
				required:true
			},
			'availability[5][time_to]':{
				required:true,
				greaterTime:"input[name='availability[5][time_from]']"
			},
			'availability[6][time_from]':{
				required:true
			},
			'availability[6][time_to]':{
				required:true,
				greaterTime:"input[name='availability[5][time_from]']"
			},
	    },
	    messages: {
	    },
	    errorPlacement: function(error, element) {
			if(element.attr("name") == "tag") {
				error.insertAfter(document.querySelector('input[name="tag"]') );
			} else {
				error.insertAfter(element);
			}
		}
    });

 	$('input[name="date_from"]').datepicker({
 		'dateFormat':'yy-mm-dd'
 	});
 	$('input[name="date_to"]').datepicker({
 		'dateFormat':'yy-mm-dd'
 	});

    $('.time').timepicker({
    	timeFormat:'H:i'
    });

    function Compare(strStartTime, strEndTime) {
	    var startTime = new Date().setHours(GetHours(strStartTime), GetMinutes(strStartTime), 0); 
    	var endTime = new Date(startTime)
    	endTime = endTime.setHours(GetHours(strEndTime), GetMinutes(strEndTime), 0);
	    if (startTime > endTime) {
	        // alert("Start Time is greater than end time");
	    	return false
	    }
	    if (startTime == endTime) {
	        // alert("Start Time equals end time");
	    	return false;
	    }
	    if (startTime < endTime) {
	    	return true
	        // alert("Start Time is less than end time");
	    }
	}
	function GetHours(d) {
	    var h = parseInt(d.split(':')[0]);
	    if (d.split(':')[1].split(' ')[1] == "PM") {
	        h = h + 12;
	    }
	    return h;
	}
	function GetMinutes(d) {
	    return parseInt(d.split(':')[1].split(' ')[0]);
	}
});