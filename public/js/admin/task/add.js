$( function() {
	$('#TaskForm').validate({
		rules: {
			'name':{
				required:true,
				minlength:3,
			},
			'description':{
				required:true,
				minlength:3
			},
	    }
    });

	$('#EditTaskForm').validate({
		rules: {
			'name':{
				required:true,
				minlength:3,
			},
			'description':{
				required:true,
				minlength:3
			},
	    }
    });

	$('.fa-trash, .fa-undo').on('click', function(){
		let url = $(this).attr('data-url');
		let msg = $(this).attr('data-message');
		let form = $('#DeletePackageForm');
		if(confirm(`Are you sure want to ${msg} the task`)) {
			form.attr('action', url);
			form.submit();
		}
	});

	$(document).on('click', '.fa-edit', function() {
    	let quadrant = $(this).attr('data-obj');
    	let url = $(this).attr('data-url');
    	let form = document.getElementById('EditTaskForm');
    	if(form) {
	    	form.reset();
	    	form.action = url;
	    	obj = JSON.parse(atob(quadrant));
	    	form.querySelector('input[name="name"]').value = obj.name
	    	form.querySelector('textarea[name="description"]').value = obj.description
    		$('#editTaskModal').modal('show');
    	}
    });

    $('#TaskDate').datepicker({
 		'dateFormat':'yy-mm-dd'
 	});
});