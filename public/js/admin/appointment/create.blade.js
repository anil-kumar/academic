(function(){
	$('#display').datetimepicker({
		format:'Y-m-d H:i'
	});
	$('#EditDisplay').datetimepicker({
		format:'Y-m-d H:i'
	});
	$('#QuoteForm').validate({
		rules: {
			'quote':{
				required:true,
				minlength:3,
			},
			'title':{
				required:true,
				minlength:3
			},
			'video':{
				required:true
			},
			'display_at':{
				required:true
			},
	    }
    });
	$('#EditQuoteForm').validate({
		rules: {
			'quote':{
				required:true,
				minlength:3,
			},
			'title':{
				required:true,
				minlength:3
			},
			'display_at':{
				required:true
			},
	    }
    });


	$(document).on('click', '.fa-edit', function() {
    	let quadrant = $(this).attr('data-obj');
    	let url = $(this).attr('data-url');
    	let form = document.getElementById('EditQuoteForm');
    	if(form) {
	    	form.reset();
	    	form.action = url;
	    	obj = JSON.parse(atob(quadrant));
	    	form.querySelector('input[name="title"]').value = obj.title
	    	form.querySelector('input[name="display_at"]').value = obj.display_at
	    	form.querySelector('textarea[name="quote"]').value = obj.quote
    		$('#editQuoteModal').modal('show');
    	}
    });
})()