!(function(t) {
    "use strict";
    t("#sidebarToggle").on("click", function(o) {
        t("body").toggleClass("sidebar-toggled");
    })
})(jQuery);
