<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserQualifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_qualifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('high_school_attending');
            $table->string('other_high_school_attending');
            $table->string('year_of_graduation');
            $table->string('high_school_start_year');
            $table->string('iep_accommodation')->nullable();
            $table->string('sport1')->nullable();
            $table->string('sport2')->nullable();
            $table->string('sport_club_team1')->nullable();
            $table->string('sport_club_team2')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_qualifications');
    }
}
