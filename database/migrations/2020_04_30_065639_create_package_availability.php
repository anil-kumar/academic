<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageAvailability extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_availabilities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('package_id');
            $table->smallInteger('day');
            $table->string('time_from')->nullable();
            $table->string('time_to')->nullable();
            $table->timestamps();
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_availabilities');
    }
}
