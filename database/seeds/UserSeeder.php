<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'role_id' => 1,
                'email' => 'admin@admin.com',
                'password' => Hash::make('password'),
            ],[
                'name' => 'anil',
                'role_id' => 2,
                'email' => 'akp@yopmail.com',
                'password' => Hash::make('password'),
            ]
        ]);
    }
}
