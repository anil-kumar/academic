<?php

use Illuminate\Database\Seeder;

class FormSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('forms')->insert([            
            [
                'title' => 'intake form',
                'file' => 'intake-form.pdf'
            ],[            
                'title' => 'surveys',
                'file' => 'survey.docx'
            ]
        ]);
    }
}
